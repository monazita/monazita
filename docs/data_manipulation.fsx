(**
---
category: Documentation
categoryindex: 1
index: 2
---
*)
(**
# Data Manipulation

In [Getting Started](/getting_started.html) we saw how to retrieve data and map it to record types.
Here we will see how to manipulated retrieved data.
This is generated from a [FSharp script](https://gitlab.com/monazita/monazita/-/blob/master/docs/data_manipulation.fsx) that you can download and run with `dotnet fsi`.

In these examples we define our `DBAction`s and immediately run them. In a real application you will probably separate your `DBAction` definitions from the code running them.
Code interacting with the database is async, and that's why you'll see async constructs (like `let!`) in the code below.
*)
(*** hide ***)

(***hide***)

#load ".lib/Setup_persons_database.fsx"
open Setup_persons_database
task {

  let! client,container = initialiseDatabase()

(*** hide ***)
  // **********************************************************************************
  // Mapping DBActions
  // **********************************************************************************
(** ### Mapping DBActions *)
(**
The `DBAction` type provides the `map` operation, which will apply the provided function to each retrieved row.
*)
(** The data retrieved will be stored in this record type: *)
(* Including a snippet from another file doest not work... include: type-personoptionalcomputer *)
(**
type PersonOptionalComputers = {
    id:PersonId;
    name:string;
    birthdate:System.DateTime;
    computers:int option;
    info:JsonValue
  }
*)
(** But we use a `DBAction.map` call to build a string describing how many computers the retrieved user possesses *)
  let stringsAction =
    DBAction.queryAction "select * from persons"
    |> DBAction.map
      (fun (p:PersonOptionalComputers) ->
        p.computers
        |> Option.map (fun n -> $"""{p.name} has {n} computer{if n>1 then "s" else ""}""")
        |> Option.defaultValue ($"{p.name} has not reported computer count")
      )
(** Notice that this transformation is defined before we even talk to the database.
Running this action would give a this `DBResult`:
```
Ok [
    "John has not reported computer count";
    "Jane has 4 computers";
    "Jim has 1 computer";
    "Jesse has 0 computer";
    "Jude has 2 computers"]
```

Let's say the `stringsAction` above is used in other places in our software, but in this case we need upper case strings.
We can define a new `DBAction` based on the `stringsAction` like this:
*)
  let uppercaseStringsAction =
    stringsAction
    |> DBAction.map (fun s -> s.ToUpper())

(** Notice again that we can define this transformation without caring about the communication with the database server.
However this corresponds to the data we need, so we can run this action with
*)

  let! uppercaseStringsResult =
    uppercaseStringsAction
    |> DBAction.run client
(**
And it results in this `DBResult`:
```
Ok [
    "JOHN HAS NOT REPORTED COMPUTER COUNT";
    "JANE HAS 4 COMPUTERS";
    "JIM HAS 1 COMPUTER";
    "JESSE HAS 0 COMPUTER";
    "JUDE HAS 2 COMPUTERS"]
```

*)
(***hide***)
  match uppercaseStringsResult with
  | Ok strings  -> printfn "Built uppercase strings %A" strings
  | Error es -> failwithf "Error retrieving data: %A" es

// DBAction.bind
(** ###DBAction.bind, aka mapping to a DBAction
DBAction.map is handy, but what if you need to map to the result of a DBAction? At that time you use `DBAction.bind`.
To illustrate this, let's say we have an action to retrieve the users with at least `n` computers:
*)
  let atLeastAction n =
    DBAction.queryAction
      "select count(*) from persons
        where
          computers is not null
          and computers > %d"
      n
(**
We also have an action to retrieve all users:
*)
  let allUsersAction =
    DBAction.queryAction
      "select * from persons"

(** How can we, for user, report how many users have more computers.
To do this, we need to retrieve the users, look at their number of computers, and query the database to see how many users have more computers.
This can be done with `DBAction`, defining all this without first talking to the database server. Here's how.
*)

  let atLeastReportAction =
    // Retrieve all users
    allUsersAction
    // For each user, run the function passed to bind.
    // That function returns a DBAction.
    |> DBAction.bind (fun (u:PersonOptionalComputers) ->
      match u.computers with
      // In this case, the user has reported a number
      | Some n ->
        // We build a new DBAction by mapping the rows
        // returned by atLeastAction to a string
        atLeastAction n
        |> DBAction.map (fun count ->
          $"""{count} users have more computers than {u.name}"""
        )
      // In this case, the user didn't report a number,
      // and we assume it is 0
      | None ->
        atLeastAction 0
        |> DBAction.map (fun count ->
          $"""{count} users have more computers than {u.name}"""
        )
    )
(** This results in a DBAction returning strings. We still didn't talk to the database server, and we could pipe this action to another `DBAction.map`, for example to
make the strings uppercase like we did above. Here we decide to run the action:
*)
  let! atLeastReportResult =
    atLeastReportAction
    |> DBAction.run client
(**
This gives this `DBResult`:
```
OK [
    "3 users have more computers than John";
    "0 users have more computers than Jane";
    "2 users have more computers than Jim";
    "3 users have more computers than Jesse";
    "1 users have more computers than Jude"]
```

When using `DBAction.bind`, you need to realise that a `DBAction` is ran for each row returned by the action we pipe to `DBAction.bind`.
In this case, we have issued a query to retrieved every user, and for each user, we issued one query to determine which other users have more computers.
Using `DBAction.bind` without caution could lead to a high number of queries being sent to your database server.
`DBAction.workOnList` might be of interest in some case.
*)
(***hide***)
  match atLeastReportResult with
  | Ok strings  -> printfn "Computers per users comparisons: %A" strings
  | Error es -> failwithf "Error retrieving data: %A" es

(**
### DBAction.workOnList

As discussed in the previous section, `DBAction.bind` can be treacherous and issue more queries than you suspect.
In the previous example, we issue one quey for each row returned, but actually by retrieving all rows we have retrieved all data to compute the end result without sending any more queries. The problem is that by using `DBAction.map`, we work row per row. In this case, we want DBAction to work on the list of retrieved rows. Luckily `DBAction.workOnList` allows us to do just that!
When we call `DBAction.workOnList` in a pipeline, the next `DBAction.map` or `bind` function will get as argument the whole list of rows returned by the previous action, and not just one rows.
Here's how it goes. We start by defining a helper function that will return the result we want when passed the list of users. It is this function that allows us to get the same result without the use of additional `DBAction`s:
*)
  let getUsersComparisons (users:PersonOptionalComputers list) =
    // Map the users list to the same string as in the previous
    // example, but without returning another DBAction.
    users
    |> List.map (fun currentUser ->
      // First computer the number of users having more computers
      // than the current one. This is done using the user list
      // and without querying the database
      let usersWithMoreComputers =
        // First get the current user's count
        let currentCount =
          currentUser.computers |> Option.defaultValue 0
        // Then filter the list
        users
        |> List.filter (fun otherUser ->
          otherUser.computers
          |> Option.map (fun otherCount ->
              otherCount > currentCount
          )
          |> Option.defaultValue false
        )
        // and cound the number of users kept
        |> List.length
      // Finally, return the string result of the List.map
      $"""{usersWithMoreComputers} have more computers than {currentUser.name}"""
    )
(** The comments in the code should make it clear.
With the helper function defined, the new pipeline is rather simple:
*)
  let improvedAtLeastReportAction =
    allUsersAction
    // Switch to working on the list of rows
    |> DBAction.workOnList
    // map now gets a list as argument
    |> DBAction.map getUsersComparisons
    // Switch back to working on items
    |> DBAction.workOnItems

(**
We call `workOnList` so that `DBAction` functions passed to `map` and `bind` get the whole list of rows as argument. With `DBAction.map`, we then build the updated list.
Note that we then call `workOnItems`, which returns the behaviour of `map` and `bind` to work on each row in turn. It also ensure we get a `DBResult<PersonOptionalComputers>` when running the action (and not a `DBResult<PersonOptionalComputers list>`).

We can then run the action as usually and get the same result, but all with issuing only one query to the database server.
*)
  let! improvedAtLeastReportResult =
    improvedAtLeastReportAction
    |> DBAction.run client
(***hide***)
  match improvedAtLeastReportResult with
  | Ok strings  -> printfn "Improved computers per users comparisons: %A" strings
  | Error es -> failwithf "Error retrieving data: %A" es

  // ********************************************************************************
  // Cleanup
  // ********************************************************************************
  do! container.DisposeAsync().AsTask()
  return 0

}
|> Async.AwaitTask |> Async.RunSynchronously
