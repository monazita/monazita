(*** hide ***)
// Not loading npgsql or loading version 8 here causes error:
// "Could not load file or assembly 'System.Text.Json, Version=8.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'."
#r "nuget: Npgsql, 7.0.1"
#r "nuget: Testcontainers.PostgreSql, 3.6.0"
#r "nuget: Monazita, 0.0.5-alpha"
// To create logger
#r "nuget: Microsoft.Extensions.Logging, 8.0.0"
#r "nuget: Microsoft.Extensions.Logging.Debug, 8.0.0"

open Testcontainers.PostgreSql
open Microsoft.Extensions.Logging
open DotNet.Testcontainers.Configurations

// for JsonValue
open FSharp.Data
// for the ? operator
open FSharp.Data.JsonExtensions

// Set logger
let (logger:ILogger) = LoggerFactory.Create(fun builder -> builder.AddDebug()|>ignore).CreateLogger("fsdocs")
TestcontainersSettings.Logger <- logger

// ********************************************************************************
// Helper functions
// ********************************************************************************
// build the postgresql container
let buildPostgresqlContainer () =
    (new PostgreSqlBuilder())
          .WithImage("postgres:16")
          .Build();
// Setup the postgresql container, ie start it and create tables
let initialisePostgresqlContainer (container:PostgreSqlContainer) = task {
    do! container.StartAsync()
}

// Setup and return the postgresql container
let setupPostgresql() = task {
    let container = buildPostgresqlContainer()
    do! initialisePostgresqlContainer container
    return container
}


//********************************************************************************
// Types
//********************************************************************************
type PersonId = PersonId of int


(* note that these snippets cannot be referenced from the script loading this file :-( *)
(* Still leaving the definition here for easier identification *)
(*** define: type-person ***)
type Person = {
    id:int;
    name:string;
    birthdate:System.DateTime;
    computers:int;
    info:JsonValue
  }
(*** define: type-personsafeid ***)
type PersonSafeId = {
    id:PersonId;
    name:string;
    birthdate:System.DateTime;
    computers:int
  }
(*** define: type-personoptionalcomputer ***)
type PersonOptionalComputers = {
    id:PersonId;
    name:string;
    birthdate:System.DateTime;
    computers:int option;
    info:JsonValue
  }
(***hide***)

let initialiseDatabase () = task {
  let! container = setupPostgresql()
  // ********************************************************************************
  // Setup container
  // ********************************************************************************
  let connectionString = container.GetConnectionString()
  let client = DB.DBClient(connectionString)
  do! client.Open()
(*** hide ***)

  // **********************************************************************************
  // Create table
  // ********************************************************************************
  let! impactedRows =
      DBAction.statementAction "create table IF NOT EXISTS persons(id SERIAL, name text, birthdate date, computers int,info jsonb)"
      |>DBAction.run client
  match impactedRows with
  | Ok _ -> printfn "Create table person"
  | Error es -> failwithf "Error creating table persons: %A" es


  // **********************************************************************************
  // Create data
  // **********************************************************************************
  let insertAction =
    DBAction.statementAction
      """insert into persons(name, birthdate, computers, info)
          VALUES
            ('John' ,'1931-03-12',null,'{"country":"US", "interests":["cars","bikes","birds"]}'),
            ('Jane' ,'1942-04-17',4,'{"country":"UK", "interests":["homes","gardening"]}'),
            ('Jim'  ,'1974-03-24',1,'{"country":"FR", "interests":["cars","fishing"]}'),
            ('Jesse','1962-05-26',0,'{"country":"DE", "interests":["bikes","fishing"]}'),
            ('Jude' ,'2001-10-05',2,'{"country":"JP", "interests":[]}')
      """
  let! impactedRows =
      insertAction
      |>DBAction.run client
  match impactedRows with
  | Ok n -> printfn "Filled table person"
  | Error es -> failwithf "Error filling table persons: %A" es
  return client,container
}
