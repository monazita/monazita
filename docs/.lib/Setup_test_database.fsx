(*** hide ***)
// ***************
// Install nugets
// ***************
// This is to use the nuget built locally. Uncomment it and update the Monazita version to load.
#i "nuget: /home/rb/gits/monazita/lib/bin/Debug/"

// Not loading npgsql or loading version 8 here causes error:
// "Could not load file or assembly 'System.Text.Json, Version=8.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'."
#r "nuget: Npgsql, 7.0.1"
#r "nuget: Testcontainers.PostgreSql, 3.6.0"
#r "nuget: Monazita, 0.0.5-alpha"
// To create logger
#r "nuget: Microsoft.Extensions.Logging, 8.0.0"
#r "nuget: Microsoft.Extensions.Logging.Debug, 8.0.0"

open Testcontainers.PostgreSql
open Microsoft.Extensions.Logging
open DotNet.Testcontainers.Configurations

// for JsonValue
open FSharp.Data
// for the ? operator
open FSharp.Data.JsonExtensions

// Set logger
let (logger:ILogger) = LoggerFactory.Create(fun builder -> builder.AddDebug()|>ignore).CreateLogger("fsdocs")
TestcontainersSettings.Logger <- logger

// ********************************************************************************
// Helper functions
// ********************************************************************************
// build the postgresql container
let buildPostgresqlContainer () =
    (new PostgreSqlBuilder())
          .WithImage("postgres:16")
          //.WithEnvironment("PGOPTIONS","-c log_statement=all")
          .WithCommand("-c","log_statement=all")
          .Build();
// Setup the postgresql container, ie start it and create tables
let initialisePostgresqlContainer (container:PostgreSqlContainer) = task {
    do! container.StartAsync()
}

// Setup and return the postgresql container
let setupPostgresql() = task {
    let container = buildPostgresqlContainer()
    do! initialisePostgresqlContainer container
    return container
}

type TestValue = {
  id: int
  value: string
}

let initialiseDatabase() = task{

  let! container = setupPostgresql()
  // ********************************************************************************
  // Setup container
  // ********************************************************************************
  let connectionString = container.GetConnectionString()
  let client = DB.DBClient(connectionString)
  do! client.Open()
  // **********************************************************************************
  // Create table
  // ********************************************************************************
  let! impactedRows =
      DBAction.statementAction "create table IF NOT EXISTS test_table(id SERIAL, value text)"
      |>DBAction.run client
  match impactedRows with
  | Ok _ -> printfn "Create table person"
  | Error es -> failwithf "Error creating table persons: %A" es

  return client,container
}

let countAction = DBAction.queryAction "select count(*) from test_table"
let countAsync(client) = async{
    let! (countResult:DBResult.DBResult<int64>) =
      countAction
      |> DBAction.run client
    match countResult with
    | Ok [n] -> return n
    | _ ->
      failwith "error in count"
      return Unchecked.defaultof<_>

}
let cleanupAction = DBAction.statementAction "delete from test_table"
let cleanup client = async {
    do! DBAction.run client cleanupAction |> Async.Ignore
}
let runAndReport client action actionName = async {
  do! action |> DBAction.run client |> Async.Ignore
  let! count = countAsync client
  printfn "%s count = %d" actionName count
  do! cleanup client
}
