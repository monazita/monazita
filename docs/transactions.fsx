(**
---
category: Documentation
categoryindex: 1
index: 5
---
*)

(*** hide ***)
#load ".lib/Setup_test_database.fsx"
open Setup_test_database

task {

  let! client,container = initialiseDatabase()
(**
# Transactions

When you issues multiple queries, it is usual to want to have all of them applied or none at all. This is achieved using transactions and savepoints, both supported by Monazita.
We base this documentation on the [tests covering transactions](https://gitlab.com/monazita/monazita/-/blob/master/tests/database/DBTestsWithoutFixtures.fs).

As we have seen in earlier examples, a `DBAction` can issue multiple statements. This is typically achieved by calling `DBAction.bind`.
Let's start by defining a helper:
*)
  let thenRun nextAction action =
      action
      |>DBAction.bind (fun _ -> nextAction)
(**

This ignores the result of the first action then runs the next one.

Assuming we have a function to define `DBActions`:
*)
  let insertActions (n:int)=
        DBAction.statementAction
          "insert into test_table(value) VALUES (%s)"
          $"value{n}"
(**
We can now build actions inserting multiple rows like this:
*)
  let insertFourAction =
    insertActions 0
    |> thenRun (insertActions 1 )
    |> thenRun (insertActions 2 )
    |> thenRun (insertActions 3 )
(**
Running this action effectively inserts 4 rows *)
(*** hide ***)
  do! runAndReport client insertFourAction (nameof insertFourAction)

(** However, if one of the actions fails, the preceding actions are applied and subsequent ones are ignores. For example :*)
  let insertWithErrorAction =
    insertActions 0
    |> thenRun (insertActions 1)
    |> thenRun (DBAction.error "This failed")
    |> thenRun (insertActions 3)
(** This effectively inserts 2 rows *)
(*** hide ***)
  do! runAndReport client insertWithErrorAction (nameof insertWithErrorAction)

(** If we want an all or none result for the DBAction, we can wrap it in a transaction *)

  let txAction =
    insertWithErrorAction
    |> DBAction.wrapInTransactionIfNeeded
(** Running this action will insert no row at all as expected from statements executed in a failed transaction.*)
(*** hide ***)
  do! runAndReport client txAction (nameof txAction)

(**
You'll have noticed that the function called is named `wrapInTransactionIfNeeded` and this name conveys two important elements of information:

* `IfNeeded`: postgresql does not support nested transactions, so only one transaction can be started.
* `wrapInTransaction`: an action is wrapped in a transaction in the sense that a transaction is started, then the action is run, and then it is committed if successful.

You don't start and commit the transaction explicitly, but you indicate which action needs to be wrapped in a transaction. As such, the transaction wrapping is usually done as the last step in a pipeline constructing an action.

## Savepoints
Postgresql does not support nested transactions, but it has [save points](https://www.postgresql.org/docs/current/sql-savepoint.html). In the words of the postgresql documentation, a savepoint `allows all commands that are executed after it was established to be rolled back, restoring the transaction state to what it was at the time of the savepoint`.

Action can be wrapped with a savepoint for rollback in case of error. In that case, the action that was wrapped with a savepoint is rolled back, but all action that were run before it are left intact.
For subsequent actions, there are 2 possibilities:

* If the action that was rolledback propagates its error, subsequent actions are not run and it the error propagates up to the wrapping transaction, the whole transaction is rolled back.
* If the action that was rolledback *does not* propagate its error, subsequent actions are run.

Here is an illustrative example:
```
  let insertWithSavepointAction =
    insertActions 0
    |> thenRun (insertActions 1 )
    |> thenRun (insertActions 2 )
    |> thenRun
      (insertActions 3
       |> thenRun (insertActions 4)
       |> DBAction.wrapWithSavePointAndRollBackForErrorButContinue
      )
    |> thenRun
      (insertActions 5
       |> thenRun (insertActions 6)
       |> DBAction.wrapWithSavePointAndRollBackForErrorButContinue
      )
    |> DBAction.wrapInTransactionIfNeeded
```

It corresponds to this database statements sorted in their order of execution:
```
START TRANSACTION
     |
insertAction 0
     |
insertAction 1
     |
insertAction 2
     |
 SAVEPOINT A
     |
insertAction 3
     |
insertAction 4
     |
ROLLBACK to savepoint A if error
     |
 SAVEPOINT B
     |
insertAction 5
     |
insertAction 6 which is in error here
     |
ROLLBACK to savepoint B if error
     |
COMMIT or ROLLBACK if error

```

Now, let's say `insertActions 4 fails`. Here is the corresponding action definition:
*)

  let insertWithSavepointAction =
    insertActions 0
    |> thenRun (insertActions 1 )
    |> thenRun (insertActions 2 )
    |> thenRun
      (insertActions 3
       // This was insertActions 4, but it fails
       |> thenRun (DBAction.error "failed action")
       |> DBAction.wrapWithSavePointAndRollBackForErrorButContinue
      )
    |> thenRun
      (insertActions 5
       |> thenRun (insertActions 6)
       |> DBAction.wrapWithSavePointAndRollBackForErrorButContinue
      )
    |> DBAction.wrapInTransactionIfNeeded
(**
As we set a savepoint, but decide to not propagate the error as we are calling `wrapWithSavePointAndRollBackForErrorButContinue`, we only have the rollback to `SAVEPOINT A` taking place, and all other actions are run and committed. This leads to the insertion of 5 rows with values `value0`, `value1`, `value2`, `value5`, `value6`
*)
(*** hide ***)
  printfn "if the savepoint is handled correctly, there are 5 rows inserted by %s." (nameof insertWithSavepointAction)
  do! runAndReport client insertWithSavepointAction (nameof insertWithSavepointAction)

(** If however we decide to progapate the error to subsequent actions, we define the action like this: *)

  let insertSafelyWithSavepointAction =
    insertActions 0
    |> thenRun (insertActions 1 )
    |> thenRun (insertActions 2 )
    |> thenRun
      (insertActions 3
       // This was insertActions 4, but it fails
       |> thenRun (DBAction.error "failed action")
       |> DBAction.wrapWithSavePointAndRollBackForError
      )
    |> thenRun
      (insertActions 5
       |> thenRun (insertActions 6)
       |> DBAction.wrapWithSavePointAndRollBackForError
      )
    |> DBAction.wrapInTransactionIfNeeded

(** Running this will cause the whole transaction to be rolled back, and no row will be inserted.
*)
(*** hide ***)

  printfn "if the savepoint is handled correctly, there are no rows inserted by %s." (nameof insertSafelyWithSavepointAction)
  do! runAndReport client insertSafelyWithSavepointAction (nameof insertSafelyWithSavepointAction)

  if System.Environment.GetEnvironmentVariable("DEBUG") = "1" then
    printfn "Debugging. You can inspect logs from the postgresql container. Press enter to continue and clean it up."
    System.Console.ReadLine() |> ignore

  do! container.DisposeAsync().AsTask()
  return 0
}
|> Async.AwaitTask |> Async.RunSynchronously
