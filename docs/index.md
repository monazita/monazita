# Monazita

Monazita is a LGPL-licensed F# library to interact with a Postgresql server by writing SQL queries using a printf format, initially developed as part of [MyOwnDB](https://www.myowndb.com) but extracted in its own project to make it usable from other projects. It is available for installation from [Nuget](https://www.nuget.org/packages/Monazita) and its source code repository is [on Gitlab](https://gitlab.com/monazita/monazita).

It uses the [Railway Oriented Programming](https://fsharpforfunandprofit.com/posts/recipe-part2/) approach.

## Introduction

You write the same SQL queries like you would when using the database server's frontend (e.g. `psql`).
You indicate the use of parameters with [printf format specifiers](https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/plaintext-formatting#format-specifiers-for-printf).

Here's an example defining a simple query:

```
let myUserId = 5
let action = DBAction.queryAction "select * from users where id = %d" myUserId
```

One of Monazita's characteristics is that it allows you to define all manipulations of the retrieved data before actually interacting with the database server.

```
let surname = "Doe"
let action =
    DBAction.queryAction "select * from users where surname = %s" surname
    |> DBAction.map (fun user -> user.firtname )
```

Being able to define all database interactions and data manipulations before actually executing it is similar to how `Async` computations are defined in F#.
You have a clean separation of concerns between defining your data retrieval and manipulations, and actually interacting with the database server.
For example this allows you to split your application code in two parts:

* a library part, where you only define data retrieval and manipulations without ever worrying about the database connection. As you don't interact with the database server, this code has no need to be async.
* an actual application part, using the library and connecting to the database. This code has to be async as database interactions are.

Executing the actions is done by `run`ning them:
```
let dbResult =
    action
    |> DBAction.run myConnection
```

Running an action gives you a `DBResult<'a>` which is actually a `Result<'a list,Errors>`, which you can pattern match on:
```
match dbResult with
| Ok l -> printfn "we retrieved this list from the database: %A" l
| Error es -> pritnfn "we got an error retrieving data: %s" (es |> Error.toString)
```
## Pros and Cons

Pros:

* Use any [SQL feature](https://www.postgresql.org/about/featurematrix/#sql) supported by the database.
* Write your data retrieval and manipulation code without having to worry about connection errors handling. This code can most probably be async-free too.

Cons:

* Data type safety of query parameters s limited by Printf placeholders. If you use a [single case DU](https://jindraivanek.hashnode.dev/f-tips-weekly-1-single-case-du) to distinguish user ids from company ids, you need to unwrap the value and pass an integer as query parameter (an alternative is to use the [StructuredDisplayFormat](https://fsharp.github.io/fsharp-core-docs/reference/fsharp-core-structuredformatdisplayattribute.html) attribute, but it is used with the `%A` format specified which does not type check the value passed).
* Parameters are positional, not named. Parameters are If you use a value twice in the query, you need to pass that value twice. This is a problem for some devs.
* No support for [batched queries](https://www.npgsql.org/doc/api/Npgsql.NpgsqlBatchCommand.html).


## Status

Although it is used in a real-world application, it is published as an alpha version because:

* it needs some cleanups after having been extracted from the [MyOwnDB code](https://gitlab.com/myowndb/myowndb)
* it is currently only used in one application by one developer, and other users might have slightly different approaches that could uncover bugs
* it has been written while learning F#, and its code could undergo some refactorings inducing API changes
* its performance hasn't been evaluated

However, note that its has proved useful and practical, handling complex queries and scenarios.
