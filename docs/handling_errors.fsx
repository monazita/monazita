(**
---
category: Documentation
categoryindex: 1
index: 4
---
*)
(**
# Dealing with errors

Monazita implements [Railway Oriented Programming](https://fsharpforfunandprofit.com/rop/).

The best illustration comes from [another post](https://fsharpforfunandprofit.com/posts/recipe-part2/):

<figure>
  <img src="/img/Recipe_Railway_Transparent.png" alt="Railway Oriented Programming illustration"/>
  <figcaption><em>&copy; Scott Wlaschin</em></figcaption>
</figure>

It illustrates clearly that when function are combined, there's a happy path in green, and an error path in red.
When an error happens on the happy path, we are redirected on the error path, where we are stuck as there's (usually) no way to return on the happy path.
It means that once we're on the error path, no action of the happy path is taken. In our case this means that once we get an error from a query, all subsequent queries are skipped.
*)

(*** hide ***)
let log(s)=
  printfn "*** LOG: %s" s
#load ".lib/Setup_test_database.fsx"
open Setup_test_database
task{
  let! client,container = initialiseDatabase()


(**
We will work with a database table that is mapped to this type:
```
type TestValue = {
  id: int
  value: string
}
```
and we define this helper function to easily insert rows in the database:
*)
  let insertValueAction (value:string):DBAction.DBAction<TestValue> =
    DBAction.queryAction
      "insert into test_table(value) VALUES (%s) RETURNING *"
      value
(**
We can then insert define one action inserting multiple rows like this:
*)
  let fourInsertsAction =
    insertValueAction "my entry"
    |> DBAction.bind (fun _ -> insertValueAction "my second entry")
    |> DBAction.bind (fun _ -> insertValueAction "my third entry")
    |> DBAction.bind (fun _ -> insertValueAction "my fourth entry")

(*** hide ***)
  do! runAndReport client fourInsertsAction (nameof fourInsertsAction)
(**
We keep it deliberately simple here to focus on the execution of multiple subsequent queries[^1]. It makes for an unrealistic example, however you might imagine that subsequent queries depend on the previous one. See the note [Dependent queries](#Dependent-queries) below for an example.

When execute the `fourInsertsAction`, we stay on the happy path as long as the queries are executed succesfully. In most case, this means that four rows will be inserted in the database.
However, when one of the queries fails, we are placed on the error path, like in this example:
*)
  let insertsWithErrorAction =
    insertValueAction "my entry"
    |> DBAction.bind (fun _ -> insertValueAction "my second entry")
    |> DBAction.bind (fun _ -> DBAction.error "failed query")
    |> DBAction.bind (fun _ -> insertValueAction "my fourth entry")
(**
`DBAction.error` returns a `DBAction` that always fails with the error message passed as argument.
Running `insertsWithErrorAction`, we start on the happy path, and stay on it until we encouter the error. This means that we insert 2 rows, and then are put on the error path.
Once on the error path, subsequent queries are not executed, and so the last action inserting `my fourth entry` is not executed.
If you want a result where all four or none of the rows are created, take a look at the [Transactions](transactions.html) sections
*)

(*** hide ***)
  do! runAndReport client insertsWithErrorAction (nameof insertsWithErrorAction)

(**
## Errors in Monazita
Errors in Monazita are stored in the opaque type `Errors` in the module `Error`.
Multiple errors can be stored in on instance of the `Errors` type.

You don't have access to the `Errors` internals, but there are [helpers](https://gitlab.com/monazita/monazita/-/blob/master/lib/Error.fsi) available.
First you can convert errors to a string or a string list:
```
val toString: Errors -> string
val toStringList: Errors -> string list
val toShortStringList: Errors -> string list
```

You can also create `Errors` with these helpers:
```
val fromString: string -> t
val fromExn: exn -> t
```

Finally some helpers let you manipulate or get information from `Errors`:
```
val join: Errors -> Errors -> Errors
val numbers: Errors -> int
val getExceptions: Errors -> exn list
```


##Handling errors

### After running queries

Until now we didn't look at the errors encountered. Let's do it now:
*)

  let! result =
    DBAction.run
      client
      insertsWithErrorAction
  match result with
  | Ok _ -> printfn "Unexpected success, we shouldn't get here"
  | Error es -> printfn "We got an error: %s" (es |> Error.toString)

(**
This will print
```
We got an error: failed query
```

This works, but a limitation is that you can define your errors handling only after the queries were run.
One benefit of Monazita is that it lets you define you data retrieval and manipulation without caring about the database connection. You do that by staying in the world of `DBAction`s.
Wouldn't it be great to be able to define some error handling just as we define data manipulation before we even connect to the database? The answer is of course a resounding yes, and Monazita lets you do it.

### Before running queries

As discussed above, one we encounter an error in a `DBAction` pipeline as the one defined in `fourInsertsAction`, we get on the error path.
And using some `DBAction` helpers, we can take some actions on the `error` path.

First, there's `DBAction.mapError` which gives you access to the error. You can for example use it to log the error:
*)
  let loggedErrorsAction =
    insertsWithErrorAction
    |> DBAction.mapError (fun es ->
      // Log the error
      log(sprintf "we encountered an error : %s" (es|>Error.toString))
      // Replace it with a user friendly message
      Error.fromString "Error message exposed to the user"
    )
(**
Keep in mind that `DBAction.mapError` only acts on the error path. So if no error is encountered when running the action, nothing will be logged as expected.
Also note that the function passed to `mapError` needs to return an error. If you don't need to change it, you can simply return the error you received as argument. In our case however, we take to opportunity to replace the error message with something we can display to the user.
Here again you see the pattern where a library makes `DBAction`s available without caring about database connections or user communications, and the app using that library takes responsability of more operational concerns.
*)
(*** hide ***)
  printfn "Running successful action and logging its errors, should print nothing"
  do! fourInsertsAction
      |> DBAction.run client
      |> Async.Ignore
  printfn "Running error action and logging its errors, should log:"
  do! insertsWithErrorAction
      |> DBAction.mapError (fun es ->
        // Log the error
        log(sprintf "we encountered an error : %s" (es|>Error.toString))
        // Replace it with a user friendly message
        Error.fromString "Error message exposed to the user"
      )
      |> DBAction.run client
      |> Async.Ignore

(**

### Getting happy again

In the illustration above, we see that once on the error path, there's no way back to the happy path. But don't despair, Monazita is here to help!
The helper `DBAction.bindError` is active on the error path only, but it lets you return an `DBAction` which, if run successfully, gets you back on the happy path.
Its use is rather exceptional, and here's the only use I encountered until now (which also illustrated the use of `DBAction.wrapError`):
```
  importAction
  // In case of error, set the import entry step to -1
  |> DBAction.bindError (fun e ->
      DBAction.statementAction "update imports set step=-1 where id=%d" (import.id|>ImportId.Get)
      |> DBAction.bind (fun _ -> DBAction.wrapError e)
  )

```

*)

(*** hide ***)
  if System.Environment.GetEnvironmentVariable("DEBUG") = "1" then
    printfn "Debugging. You can inspect logs from the postgresql container. Press enter to continue and clean it up."
    System.Console.ReadLine() |> ignore
  // ********************************************************************************
  // Cleanup
  // ********************************************************************************
  do! container.DisposeAsync().AsTask()
  return 0

}
|> Async.AwaitTask |> Async.RunSynchronously
(**

## Notes

### Dependent queries
<a name="dependent_queries"></a>
Here is an example where the second query is dependent on the first one.
```
  let dependentInsertAction =
    insertValueAction "my entry"
    |> DBAction.bind
      (fun (row:TestValue) ->
        DBAction.queryAction
          "insert into related_table(value, value_id)
            VALUES (%s, %d) RETURNING *"
          "related_value"
          row.id
      )
```
The fact we use the inserted row's id means we can only execute the second query after the first one.
We don't use this in the examples though as it would make the code more complex without added value for the subject being discussed.

*)
