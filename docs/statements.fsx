(**
---
category: Documentation
categoryindex: 1
index: 3
---
*)
(**
# Statements

Until now we have queried the database without inserting or updating data. But it is of course possible to issue statements to update, insert or delete rows in the database using Monazita.
A statement is defined using `DBAction.statementAction` and is similar to `DBAction.queryAction`, except that when successful it returns the number of rows impacted wrapped in the single case DU type `ImpactedRows`.

*)
(*** hide ***)

#load ".lib/Setup_persons_database.fsx"
open Setup_persons_database

task {
  let! (client,container) = initialiseDatabase()
  // **********************************************************************************
  // Inserting data
  // **********************************************************************************
(** ## Inserting and Updating data *)
(**
We can insert data like this:
*)
  let insertStatementAction =
    DBAction.statementAction
      "insert into
        persons(name,birthdate,computers,info)
       values
        (%s,%A,%d,'{}')"
      "Jef"
      (System.DateOnly.Parse("2005-05-23"))
      5
(**
We pass arguments like we did for `queryAction`s.
But there's some interesting information here regarding passing arguments of types not handled by printf place holders.
The column `birthdate` is of type `date` and we need to pass it a value of that type. However, printf does not have a placeholder for date values and the work-around is to use the "any value" placeholder `%A`. As a consequence, this is not type-checked at compilation time, but would generated a runtime error if you passed a string value.
*)
(** This action is can be run like query actions: *)
  let! insertStatementResult =
    insertStatementAction
    |> DBAction.run client
(**
The value we get back however is the number of rows impacted:
*)
  match insertStatementResult with
  | Ok [DBTypes.ImpactedRows 1 ]  -> printfn "Inserted one row as expected"
  | Error es -> failwithf "Error retrieving data: %A" es
  | otherData -> failwithf "Unexpected value received after insert: %A" otherData

(** ### JSON columns
In the previous example, we inserted an empty object in the json column `info`. Let's see how we can insert JSON data.
Using `psql`, we can pass JSON values in a string, for example:
```
UPDATE persons
  set info = '{"country":"LU","interests": ["computers"] }'
  where name='Jef';
```
Using Monazita, we cannot pass a string a the JSON value.

*)

  let errorStatementAction =
    DBAction.statementAction
      "update persons set info=%s where name='Jef'"
      """{"country":"LU", "interests":["computers"]}"""

(** Running this action would give the error `column "info" is of type jsonb but expression is of type text`.
The error message give a hint as to the solution, which is to cast our string to a `jsonb` value like this:
*)
  let jsonUpdateAction =
    DBAction.statementAction
      "update persons set info=%s::jsonb where name='Jef'"
      """{"country":"LU", "interests":["computers"]}"""
  let! jsonUpdateResult =
    DBAction.run client jsonUpdateAction
(*** hide ***)
  match jsonUpdateResult with
  | Ok [DBTypes.ImpactedRows 1]  -> printfn "update successful"
  | Error es -> failwithf "Error retrieving data: %A" es
  | otherData -> failwithf "Unexpected value received after insert: %A" otherData

(** In your statements, you can make use of all json functions provided by postgresql. Here is how you can update just the key `country` of the json object in the column `info`.*)
  let jsonUpdateCountryAction =
    DBAction.statementAction
      "update persons set info=info::jsonb || %s::jsonb where name='Jef'"
      """{"country":"BE"}"""

(** of if you don't want to build a json object string and let postgres build it: *)

  let jsonUpdateCountryAction =
    DBAction.statementAction
      "update persons
          set info=info::jsonb || jsonb_build_object('country', %s)
         where name='Jef'"
      "BE"

(*** hide ***)
  let! jsonUpdateCountryResult =
    DBAction.run client jsonUpdateCountryAction

  match jsonUpdateCountryResult with
  | Ok [DBTypes.ImpactedRows 1]  -> printfn "update of country successful"
  | Error es -> failwithf "Error retrieving data: %A" es
  | otherData -> failwithf "Unexpected value received after insert: %A" otherData

(** ##Deleting data
Deleting data is done similarly by writing `delete` statement, for example:
```
DBAction.statementAction
  "delete from persons where name = %s"
  "Jef"
```
*)

(**
# Inserting data with a query
A statement returns the number of rows impacted, but with Postgresql we can get back the row inserted with this syntax:
```
INSERT
  INTO persons(name,birthdate,computers,info)
  VALUES ('Jenni','2008-09-21',1,'{}')
  RETURNING *
```

This syntax can be used with Monazita, however as it returns a rows, it must be a query and not a statement, and hence requires the use of `DBAction.queryAction`.
*)

(*** hide ***)

  // ********************************************************************************
  // Cleanup
  // ********************************************************************************
  do! container.DisposeAsync().AsTask()
  return 0

}
|> Async.AwaitTask |> Async.RunSynchronously
