(**
---
category: Documentation
categoryindex: 1
index: 1
---
*)
(**
# Getting Started

This is generated from a [FSharp script](https://gitlab.com/monazita/monazita/-/blob/master/docs/getting_started.fsx?ref_type=heads) that you can download and run with `dotnet fsi`.
In these examples we define our `DBAction`s and immediately run them. In a real application you will probably separate your `DBAction` definitions from the code running them.
Code interacting with the database is async, and that's why you'll see async constructs (like `let!`) in the code below.
*)
(*** hide ***)

#load ".lib/Setup_persons_database.fsx"
open Setup_persons_database
// for JsonValue
open FSharp.Data
// for the ? operator
open FSharp.Data.JsonExtensions

task {
  // We receive the client here, but don't use it as we show below how to initialise a client.
  let! _client,container = initialiseDatabase()
  // ********************************************************************************
  // Setup container
  // ********************************************************************************
  let connectionString = container.GetConnectionString()
(** ## Database connection
As we run our `DBAction`s, we need to connect to the database specified using a connection string like for example `Host=127.0.0.1;Port=32769;Database=postgres;Username=postgres;Password=postgres`

The connection string is used to initialise an instance of `DBClient` like this:
*)
  let client = DB.DBClient(connectionString)
  do! client.Open()
(*** hide ***)
  // **********************************************************************************
  // Retrieve count
  // **********************************************************************************
(**
## Retrieving data
  A DBAction can be a query, which is retrieving data, or a statement, which is inserting or updating data.
  When a DBAction is ran it returns a `DBResult<'t>` where `'t` is the type the returned data is mapped to.
  A statement always returns the number of rows impacted (which is `DBTypes.ImpactedRows of int`).
  A query will return data mapped to a type we suggest to the type inference engine.
  The type inference needs some hint to know which data type is retrieved. In more complex code,
  the type can often be inferred from the way the retrieved data is used. In this very simple case,
  we need to explicitly specify the type wrapped by the DBResult. If we don't here, the inferred type
  will be int32 due to the use of the retrieved data as argument to `printfn "Found %d persons"`.
  *)
  let! (countResult:DBResult.DBResult<int64>) =
    DBAction.queryAction "select count(*) from persons"
    |> DBAction.run client

(** A `DBresult<'a>` is simply a `Result<'a list,Errors>`, and can be pattern-matched.
  In this case we expect only one row to be returned and consider more rows returned to be an error.
  *)
  match countResult with
  | Ok [n]  -> printfn "Found %d persons" n
  | Ok l -> failwithf "Unexpected number of rows returned: %A" l
  | Error es -> failwithf "Error counting persons: %A" es
(*** hide ***)

  // **********************************************************************************
  // Map to record
  // **********************************************************************************
  // We need to hint the type inferrence which record we want to map the data to.
(**
## Mapping to a record type
  When retrieving database rows, we can map it to FSharp records.
  We first need to define the type:
  *)
(**
```
type Person = {
    id:int;
    name:string;
    birthdate:System.DateTime;
    computers:int;
    info:JsonValue
  }
```
*)
(** We can then specify that type as type argument to DBResult to indicate the retrieved rows
   need to be mapped to a Person record.
   We can pass parameters to the query, and these are correctly escaped to prevent sql injection.
   *)
  let! (onePersonDBResult:DBResult.DBResult<Person>) =
    DBAction.queryAction
      "select * from persons
        where
          computers is not null
          and name <> %s
        limit 1"
      "Jim"
    |> DBAction.run client

  match onePersonDBResult with
  | Ok [p]  -> printfn "Retrieved person %A" p
  | Ok l -> failwithf "Unexpected number of rows returned: %A" l
  | Error es -> failwithf "Error retrieving one person: %A" es

(** As mentioned above, a DBResult<'a>' is actually a Result<'a list, Errors>.
  We can convert it to a single Result when we retrieve only one row, which simplifies the
  pattern matching. Note however that if multiple rows were retrieved, a runtime exception
  will be thrown.
  *)
  let onePersonResult =
    onePersonDBResult
    |> DBResult.toSingleResult
  match onePersonResult with
  | Ok p  -> printfn "Retrieved single person %A" p
  | Error es -> failwithf "Error retrieving one person: %A" es

(*** hide ***)
  // **********************************************************************************
  // Map to record with Id type
  // **********************************************************************************
(**
### Mapping to a Single Case DU field
It is a common practice to map ids to single case Discriminated Union types, and this is
  supported.
  If a field of the record type to map to is a single DU type, it will be handled correctly.
  For example, if we define an id type
```
type PersonId = PersonId of int
```
  *)
(** and use it in the record type definition: *)
(**
```
type PersonSafeId = {
    id:PersonId;
    name:string;
    birthdate:System.DateTime;
    computers:int
  }
```
*)
(** We can simply specify that our retrieved rows need to be mapped to that type: *)
  // We need to hint the type inferrence which record we want to map the data to.
  let! (onePersonResult:DBResult.DBResult<PersonSafeId>) =
    DBAction.queryAction
      "select * from persons where computers is not null limit 1 "
    |> DBAction.run client


  match onePersonResult|> DBResult.toSingleResult with
  | Ok p  -> printfn "Retrieved person %A" p
  | Error es -> failwithf "Error retrieving one person: %A" es

(*** hide ***)
  // **********************************************************************************
  // Map to record with option type
  // **********************************************************************************
(** ### Mapping to an option field *)
(** We can also map columns that can hold null values to an option type.
 The computers in column can have null values, and we can map it to an `int option` type.
 Here is the record type with the `computers` field an `option`:
     *)
(**
```
type PersonOptionalComputers = {
    id:PersonId;
    name:string;
    birthdate:System.DateTime;
    computers:int option;
    info:JsonValue
  }
```
*)
(** By giving a type hint we map the rows retrieved to that record type:
*)
  // We need to hint the type inferrence which record we want to map the data to.
  let! (onePersonResult:DBResult.DBResult<PersonOptionalComputers>) =
    DBAction.queryAction "select * from persons limit 1"
    |> DBAction.run client
(***hide***)
  match onePersonResult|> DBResult.toSingleResult with
  | Ok p  -> printfn "Retrieved person %A" p
  | Error es -> failwithf "Error retrieving one person: %A" es

// Anonymous records
(** ### Mapping to anonymous records *)
(** You can map rows to anonymous records. This is particularly handy when you use the retrieved data locally or when
you only want to retrieve a specific subset of columns of the table.
*)
  let! (anonymousRecordResult:DBResult.DBResult<{| id: PersonId; name: string|}>) =
    DBAction.queryAction "select id, name from persons limit 1"
    |> DBAction.run client
(*** hide ***)
  match anonymousRecordResult|> DBResult.toSingleResult with
  | Ok p  -> printfn "Retrieved in an anonymous record person %A" p
  | Error es -> failwithf "Error retrieving one person with anonymous record: %A" es

  // ********************************************************************************
  // DBAction.map
  // ********************************************************************************
(** DBActions can be manipulated before the quey is sent to the database. For example, we can map
its rows by calling `DBAction.map`: *)
  let! squareJaneComputers =
    DBAction.queryAction "select computers from persons where name='Jane'"
    |> DBAction.map (fun computers -> computers * computers)
    |> DBAction.run client
(***hide***)
  match squareJaneComputers with
  | Ok [p]  -> printfn "Retrieved Jane's square of computers count: %A" p
  | Ok l -> failwithf "Unexpected number of rows returned: %A" l
  | Error es -> failwithf "Error retrieving one person: %A" es

  // ********************************************************************************
  // Working with json
  // ********************************************************************************
(** ### Working with JSON columns *)
(** Postgresql, like other databases, has support for storing JSON values and we can easily
map json columns to FSharp.Data's `JsonValue`. In the next example we map our rows to the
record type `PersonOptionalComputers` as defined above. We can then access fields of the JSON
object stored in the column `info` as a `JsonValue`. Note that the `?` operator is available after loading
the [JSON extension](https://fsprojects.github.io/FSharp.Data/library/JsonValue.html#Using-JSON-extensions).

*)
  let! personCountries =
    DBAction.queryAction "select * from persons"
    |> DBAction.map
      (fun (person:PersonOptionalComputers) ->
        (person.name , person.info?country.AsString() )
      )
    |> DBAction.run client

(***hide***)
  match personCountries with
  | Ok l ->
    l
    |> List.iter (fun (name,country) -> printfn "%s is from country code %s" name country)
  | Error es -> failwithf "Error retrieving data: %A" es

(** And we have access to all SQL facilities to work with the json column. In this example we use postgresql's `?` operator to test if a value is present in the json array. *)
  let! carLovers =
    let interest = "cars"
    // We can use json facilities offered by postgresql in our queries
    DBAction.queryAction
      "select * from persons where (info->'interests') ? %s " interest
    |> DBAction.map (fun (person:PersonOptionalComputers) -> person.name)
    |> DBAction.run client
(*** hide ***)
  match carLovers with
  | Ok l ->
    l
    |> List.iter (fun name -> printfn "%s loves cars" name )
  | Error es -> failwithf "Error retrieving data: %A" es
  // ********************************************************************************
  // Cleanup
  // ********************************************************************************
  do! container.DisposeAsync().AsTask()
  return 0

}
|> Async.AwaitTask |> Async.RunSynchronously
