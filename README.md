# Intro

Monazita is an F# library to query your Postgresql database by writing SQL queries in a printf format
that was extracted from [MyOwnDB](https://www.myowndb.com). As such, although the project is still only
in alpha version, its code is heavily used in an application deployed in production.

# Documentation

See [https://monazita.gitlab.io/monazita/](https://monazita.gitlab.io/monazita/).

# License
Monazita is published under the LGPLv3 license, which lets you use it in proprietary code, and only requires you to publish changes you make to this library itself.
