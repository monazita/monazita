SHELL := /bin/bash
DOC_OUTPUT_DIR = public

.DEFAULT_GOAL := show-help

## Build the library. Useful in development with a file watcher to ensure the code builds.
build:
	dotnet build lib/monazita.fsproj

## Build the tests. Useful in development with a file watcher to ensure the tests build (without checking they pass).
build-tests:
	dotnet build tests/tests.fsproj

## Run tests.
## Select tests to run by setting them in the FILTER environment variable.
## example: FILTER="DBAction.map" make test
test:
	FILTER="$(FILTER)" && bash -c 'dotnet test tests/tests.fsproj $${FILTER:+--filter "$$FILTER"}'

## Build nupkg at lib/bin/Release/monazita.*.nupkg
nupkg:
	dotnet pack lib/monazita.fsproj

## Publish nupkg to nuget. Requires the nuget api key to be in the API_KEY environment variable.
## The API_KEY env var is normally set in .envrc, which requires direnv.
publish: nupkg
	# publish only most recent nupkg found
	dotnet nuget push $$(ls -t lib/bin/Release/Monazita.*.nupkg|head -n 1) --api-key $${API_KEY} --source https://api.nuget.org/v3/index.json

## Delete files built
clean:
	rm -rf {lib,tests}/{bin,obj}

.PHONY: clean-doc build-doc watch-doc serve-doc

## Start http server on port 8000 in directory output/ to serve locally built documentation
serve-doc:
	cd $(DOC_OUTPUT_DIR) && python -m http.server

## Delete locally built documentation directory
clean-doc:
	rm -rf $(DOC_OUTPUT_DIR)

## Build documentation found in docs/ and generate result in output/
build-doc:
	# The path to CSS, set based on the root parameter value, needs to be absolute as
	# it is used from the root directory and from `/reference` for API docs pages.
	# On gitlab it is hosted under `/monazita`, but when built locally it is served from `/`
	if [[ "$$CI" = "true" ]]; then \
		root="/monazita/"; else root="/"; \
	fi ; \
	echo "root=$$root"; \
	dotnet fsdocs build --output $(DOC_OUTPUT_DIR) --parameters root "$$root" --eval

## watch changes, rebuild doc and restart the local http server listening on port 8000
# It is necessary to restart the http server because we delete the directory in which is was starte
watch-doc:
	watchexec --watch docs/ -r make clean-doc build-doc serve-doc


# From https://gist.github.com/klmr/575726c7e05d8780505a
# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: show-help
show-help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) == Darwin && echo '--no-init --raw-control-chars')
