# Changelog

The intent is to have all notable changes to this project documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com).

## [0.0.10-alpha] - 2023-03-04

### Changed

- Ensure we do not have dependency on FSharp.Core 8, even indirect.

## [0.0.9-alpha] - 2023-03-02

### Changed

- Pin FSharp.Core version as explained [here](https://github.com/bit-badger/Giraffe.Htmx/issues/8#issuecomment-1873597252) making the lib usable in .Net 6 apps.

## [0.0.8-alpha] - 2023-03-02

### Changed

- Previous version was erroneously built with .Net 8, this reverts back to .Net 6.

## [0.0.7-alpha] - 2023-03-02

### Changed

- Updates of multiple dependencies

## [0.0.6-alpha] -2024-02-22

### Security

- Update to NewtonSoft.Json 13.01 to fix [vulnerability](https://github.com/advisories/GHSA-5crp-9r3c-p9vr).

## [0.0.5-alpha] -2024-02-22

### Added

- `DBAction.wrapWithSavePointAndRollBackForErrorButContinue` which wraps a transaction with a savepoint and rolls it back in case of error, but does not propagate its error so subsequent actions in the pipeline are still run.
- Support singular table names for queries returning a record and trying to build the Id type name based on the table name.

## [0.0.4-alpha] - 2024-01-03

### Changed

- Upgrade Npgsql to 8.0.3


## [0.0.3-alpha] - 2024-01-03

### Changed

- Upgrade Npgsql to 7.0.6
- Remove DBClient.DB.DebugInConsole as Npgsql logging was overhauled, and that function was not used anyway.
- Update tests to monazita renames, no change in behaviour


## [0.0.2-alpha] - 2024-01-01

### Added

- The Makefile now has a clean target.

### Changed

- Delete AppConfig. Were not used, but remnants of MyOwnDB code.
- Moved Utils to namespace Monazita to make it usable in MyOwnDB, where there's also a Utils.
- Renamed fsproj to Monazita and set AssemblyName to Monazita. Both seemed necessary to make it usable in MyOwnDB


## [0.0.1-alpha] - 2023-12-31

### Added

- Copied file over from [MyOwnDB](https://gitlab.com/myowndb/myowndb) and make them compile and pass tests.
- Self-documenting Makefile.
- .envrc setting API_KEY env variable. Only useful for maintainers publishing to nuget.org.
