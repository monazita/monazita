namespace MyOwnDB
open System.Collections
module Extensions=


    type IEnumerator  with
        member self.toSeq() = seq { while self.MoveNext() do (self.Current)}