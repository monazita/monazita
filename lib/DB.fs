module DB

// Ideally this file should be put in the MyOwndDBLib namespace,
// but I'd rather look at this change later, so in the meantime
// I just open the namespace to be able to do logging with
// eg Log.Verbose
open MyOwnDBLib.Logging
// for pg access
open System.Dynamic
open System.Collections.Generic
open Npgsql


open System
// for DataTable
open System.Data
open DBHelpers
open DBTypes

// for FSHarpType and FSharpValue
open Microsoft.FSharp.Reflection


// Inspiration from https://gist.github.com/vbfox/1e9f42f6dcdd9efd6660
/// Code needs to instanciate this class to get a connection to the database
/// from the connection pool.
/// The code needs to open and close the connection itself.
/// You could check this by looking at the value of pg_connection.ProcessID: it
/// is different for each instance.
type DBClient(connectionString:string) =
    // build npgsql connection string
    let connString = connectionString

    // We allow the storage of configuration items in the DBClient.
    // This is a hack, but saves a huge amount of time. When I developed the DBAction reader
    // monad I had zero experience with it, and rather than setting an environment as the
    // argument of the run function, I set the DBClient instance. When implementing file attachments,
    // it appeared that a DBAction (see DetailValue.createAction) needed some config to know where to copy
    // files from and to. This can easily implemented by passing it in the reader monad's environment, but in
    // our case it is the DBClient instance. Rather than starting a refactor, I add some config items in the
    // DBClient itself.
    // FIXME: remove this configMap from DBClient and implement a proper environment for DBAction. (That environment
    // would include the dbclient instance and the config map)
    let mutable configMap = Map.empty<string,string>

    // private function to connect to the database
    let connect () =
        // connect and return connection object
        new NpgsqlConnection(connString)

    /// Define private connection with its value obtained from call to connect
    /// This is the result of calling NpgsqlConnection.Open() and thus is a connection
    /// gotten from the pool
    let connection = connect ()
    let mutable (transaction: NpgsqlTransaction option) = None
    let mutable (savePoints: string list) = []


    member _.ConnectionString() = connString

    // Function to get and set config items
    member _.SetConfigItem(k:string,v:string) =
        configMap <- configMap.Add(k,v)
    member _.GetConfigItem(k:string) =
        configMap.TryFind(k)
    member _.GetConfigMap() =
        configMap

    /// open and close for multiple queries or starting a transaction over multiple actions
    member this.Open() = async {return! connection.OpenAsync()|> Async.AwaitTask }

    member this.Close() = async {return! connection.CloseAsync() |> Async.AwaitTask}

    // opens a connection if needed, and return a function handling the closing to be called after using the connection
    member this.OpenIfNeeded() =async {
        if this.IsOpen() then
            // if client is open, we are not responsible for its closing
            return (fun () -> async {return ()} )
        else
            // if the client is not open, we open it and return a function
            // to be called to close it
            do! this.Open()
            return (fun () -> async {do! this.Close()} )
    }
    member this.BeginTransaction() = async {
        let! tx = connection.BeginTransactionAsync().AsTask() |> Async.AwaitTask
        transaction <- Some tx
    }

    member this.Connection() = connection
    member this.Transaction() = transaction

    member this.Commit() = async {
        match transaction with
        | None -> ()
        | Some t ->
            let! commitTask =  (t.CommitAsync() |> Async.AwaitTask)
            transaction <- None
            return commitTask
    }

    member this.IsInTransaction() =
        match transaction with
        | None -> false
        | Some _ -> true

    /// Savepoints and rollback functions
    // Set a savepoint, beginning a transaction if none is active yet
    // The name of the savepoint is set automatically based on the current time
    member this.TrySetSavePoint(name: string): Async<DBResult.DBResult<string>> = async{
        match List.tryFind ((=) name) savePoints with
        | Some _ -> return DBResult.error (sprintf "Savepoint %s already exists" name)
        | None ->
            match transaction with
            | None -> return! this.BeginTransaction()
            | _ -> ()

            transaction
            |> Option.iter (fun tx -> tx.Save(name))
            savePoints <- name :: savePoints
            return Ok [ name ]
    }

    member this.SavePoints() = savePoints

    // Rollback the active transaction, if any.
    member this.TryRollback() : Async<DBResult.DBResult<unit>>= async {
        match transaction with
        | None -> return DBResult.error "No active transaction"
        | Some t ->
            try
                savePoints <- []
                transaction <- None
                let! v =  t.RollbackAsync() |> Async.AwaitTask
                return Ok [ v ]
            with e -> return DBResult.error (e.ToString())
    }

    // Rollback to first savepoint, or whole transaction if no savepoint was set
    member this.TryToFirstSavePointOrRollback() : Async<DBResult.DBResult<unit>>=
        match savePoints with
        | [] -> this.TryRollback()
        | h :: t -> this.TryToSavePoint h

    // Try to rollback to the savepoint.
    member this.TryToSavePoint(name: string): Async<DBResult.DBResult<unit>> = async {
        // matching on savepoints should be enough, no need to match on transaction, as
        // having a savepoint should mean we have a transaction
        match List.tryFindIndex (fun v -> v = name) savePoints with
        | Some i ->
            savePoints <- savePoints |> List.skip (i + 1)
            let! r = transaction
                    // couldn't get it to work with Option.iter
                    //|> Option.iter (fun tx -> tx.RollbackAsync name |> Async.AwaitTask )
                    |> function
                       | Some tx -> tx.RollbackAsync name |> Async.AwaitTask
                       | None -> async { return () }
            return Ok [ r ]
        | None -> return DBResult.error (sprintf "Savepoint %s not found" name)
    }

    // is this connection already open?
    member this.IsOpen() = connection.State.HasFlag ConnectionState.Open

    // a function wrapping a function sending a query or statement to postgresql
    // this ensure preparation and exception handling is done identically for all query types
    member this.queryWrapper<'Rec> (f: NpgsqlCommand -> Async<DBResult.DBResult<'Rec>>) (query: string)
           (mappings: Map<string, obj>): Async<DBResult.DBResult<'Rec>> = async {
        try
            let mustOpenConnection = not (this.IsOpen())
            try
                if mustOpenConnection then connection.Open()
                // join transaction if needed
                // We need to assign with let and not use
                // with use, the cmd is disposed of too early and causes an error "Cannot access a disposed object.  Object name: 'Npgsql.NpgsqlCommand'"
                let cmd =
                    if this.IsInTransaction()
                    then new NpgsqlCommand(query, connection, this.Transaction() |> Option.get)
                    else new NpgsqlCommand(query, connection)

                // add command parameters according to mappings
                let _ =
                    mappings
                    // null is not handler by npgsql, and need to be replaced by DBNull.Value
                    |> Map.map (fun k v -> if isNull v then (box DBNull.Value) else v)
                    |> Map.map (fun k v -> cmd.Parameters.AddWithValue(k, v))

                // build return value
                // the way we execute the query (Reader or Scalar) depends on the return type
                Log.Verbose("issuing query {Query} with params {Mappings}", query, mappings)
                return! (f cmd)
            finally
                if mustOpenConnection then connection.Close()
        // Set exception received as inner exception of the exception we instanciate to
        // give more info about the query that failed
        with e ->
                MyOwnDBLib.Logging.Log.Error(sprintf "Error executing query %s with mappings %A. Look at inner exception for details: %A" query mappings e)
                return DBResult.errorFromExn (new Exception(sprintf "Error executing query %s with mappings %A. Look at inner exception for details" query mappings, e))
        }


    // Execute a statement, the result is the number of rows impacted.
    // The statement is build with PrintfFormat strings
    member this.executeStatement  (query:string) (mappings:Map<string,obj>): Async<DBResult.DBResult<ImpactedRows>> =
        async {
        return! (this.queryWrapper QueryHandlers.statementHandler query mappings)
        }

    // Execute a statement, the result is the number of rows impacted.
    // The statement references record fields with the @field syntax.
    // The statement does not return rows
    member this.executeStatementOnRecord(query, r): Async<DBResult.DBResult<ImpactedRows>> =
        // Create the mappings based on the record instance 'r' we are passed.
        // We need to map the values in the record 'r' to their Npgql datatype equivalent to ensure it can be
        // store in the database correctly.
        let fields = FSharpType.GetRecordFields(r.GetType())
        let values = FSharpValue.GetRecordFields r

        let mappings =
            Array.zip fields values
            |> Array.fold (fun map (k, v) ->
                let pgValue = recordFieldToPg (box v)
                map |> Map.add k.Name pgValue) Map.empty
        async {
        return! this.queryWrapper QueryHandlers.statementHandler query mappings
        }

    /// Issue a query
    /// A query returns a list of records of type 'Rec.
    member this.q<'Rec>(query: string, mappings: Map<string, obj>): Async<DBResult.DBResult<'Rec>> =
        async {
        return! this.queryWrapper QueryHandlers.recordQueryHandler query mappings
        }

    // returns the data in a DataTableInstance
    // useful for crosstab queries where we don't have record types for the rows
    member this.qDataTable(query: string, ?mappings: Map<string, obj>): Async<DBResult.DBResult<DataTable>> =
        let mappings =
            defaultArg mappings (new Map<string, obj>([]))
        async {
        return! this.queryWrapper QueryHandlers.datatableQueryHandler query mappings
        }
    // Needed for ASP.Net Core to clean up connections handed code with DI
    interface IAsyncDisposable with
        member self.DisposeAsync() =
            task {
                if self.IsOpen() then
                    do! self.Close()
            }|>System.Threading.Tasks.ValueTask
