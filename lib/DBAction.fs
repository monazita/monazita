module DBAction
/// A reader monad for issuing database queries.

open System.Text.RegularExpressions
open System.Collections.Generic
// For DataTable
open System.Data
// for reflection in single insert actions
open Microsoft.FSharp.Reflection
// for Type
open System
// for constructing id union type case from table name
open Humanizer

open FSharpPlus

//For JSON conversion
open Newtonsoft.Json

/// A DBAction is a discriminated union type holding a function
/// of one argument (the database client).
/// Query actions return a DBResult<'Record>,
/// Statement actions return a DBResult<int>
type DBAction<'a> =
    | Query of (DB.DBClient -> Async<DBResult.DBResult<'a>>)
    | Statement of (DB.DBClient -> Async<DBResult.DBResult<'a>>)

/// Issue the DBAction query to client.
let run<'Result> (client: DB.DBClient) (query: DBAction<'Result>) =
    match query with
    | Query q -> q client
    | Statement s -> s client

let config: DBAction<Map<string,string>> = DBAction.Query (fun dbclient -> async {return DBResult.retn (dbclient.GetConfigMap())})

/// Execute the DBAction-returning function f, and pass the result to <c>run client</c>
/// This is useful as generic DBAction values need an explicit type annotation as a variable cannot
/// be left generic:
///   let users:DBAction.DBAction<User.User> = DBAction.queryAction = "select * from users limit 10"
/// Defining this in a generic function works fine however:
///   let getTenUsersFunction()= DBAction.queryAction "select * from users limit 10"
let execute (client: DB.DBClient) (f: unit -> DBAction<'a>) = run client (f ())

//******************************************************************************
// Functions implementing the Reader Monad

// Returns an action return a DBResult wrapping the value v
// Can be considered as a contructor for wrapping a value
let retn v =
    let newAction (client: DB.DBClient) = async { return DBResult.retn v }
    DBAction.Query newAction

// Returns an action yielding a DBResult wrapping the list passed as argument.
let wrapList v =
    let newAction (client: DB.DBClient) = async { return DBResult.wrapList v }
    DBAction.Query newAction

// Takes an action yielding a DBResult whose values are lists of 'a, and makes it an action
// yielding a DBResult whose values are 'a.
let flatten (a:DBAction<'a list>)=
    let newAction (client: DB.DBClient) = async {
        let! r = run client a
        let v = r |> DBResult.flatten
        return v
    }
    DBAction.Query newAction

// returns a DBAction lalways returning a DBResult.Error
let error (msg: string) =
    let newAction (client: DB.DBClient) = async { return DBResult.error msg }
    DBAction.Query newAction

let wrapError (e:Error.Errors) =
    let newAction (client: DB.DBClient) = async { return DBResult.DBResult.Error e }
    DBAction.Query newAction

// "neutral" value, action that returns a zero DBResult
let zero =
    DBAction.Query(fun (client: DB.DBClient) -> async { return DBResult.zero })

// apply handles wrapped raw functions
let apply (fa: DBAction<'a -> 'b>) (va: DBAction<'a>) =
    let newAction (client: DB.DBClient) =
        async {
        let! v = run client va
        let! f = run client fa
        return DBResult.apply f v
        }

    DBAction.Query newAction

/// map handles raw unwrapped functions
let map (f: 'a -> 'b) (action: DBAction<'a>) =
    let newAction (client: DB.DBClient) =
        async {
        let! r1 = run client action
        return DBResult.map f r1
        }

    DBAction.Query newAction

/// mapError handles mapping the error
let mapError (f: Error.Errors -> Error.Errors) (action: DBAction<'a>) =
    let newAction (client: DB.DBClient) =
        async {
        let! r1 = run client action
        return DBResult.mapError f r1
        }

    DBAction.Query newAction
/// bind handles functions taking a raw value, and returning a wrapped value
let bind (f: 'a -> DBAction<'b>) (action: DBAction<'a>): DBAction<'b> =
    let rec newAction (client: DB.DBClient): Async<DBResult.DBResult<'b>> = async {
        // action gives us a DBResult holding a list of results.
        // we need to apply f to the items in the list inside the DBResult
        // As this is wrapped in Async, we need to return a single  Async wrapping a single <DBResult<'b>>.
        // We do as below, otherwise we easily get wrong results such as Async<List<DBResult<_>>>
        let! va = run<'a> client action
        // This allows to return a single Async
        let (<*>) = Async.apply
        // This allows to return a single DBResult
        // we need to merge the lists inside the 2 DBResults, hence the use of DBResult.merge
        let cons head tail = DBResult.merge head tail
        // the folder works on a DBAction and an async DBResult and returns an Async DBResult
        let folder head tail = async {
            //async wrapped DBResult.merge  get DBResult for (f head)    already folded part
            //            v                        v                        v
            return! async {return cons} <*> (run<'b> client (f head)) <*> tail
        }
        match va with
        | Error es -> return (DBResult.DBResult.Error es)
        | Ok l -> let r = List.foldBack folder l (async{return DBResult.zero})
                  return! r
    }

    DBAction.Query newAction

// This allows us to transform the Error in a Ok, eg for mapping an authentication error to a boolean
let bindError (f:Error.Errors->DBAction<_>) (action:DBAction<_>) =
    let newAction client = async {
        let! v = run client action
        return! ( match v with
                 | Ok _ -> async { return v}
                 | Error es -> async {
                    let! v2 = run client (f es)
                    return v2
                 }
        )
    }
    DBAction.Query newAction

let merge (actionA:DBAction<'a>) (actionB:DBAction<'a>) =
    let newAction (client: DB.DBClient) =
        async {
        let! a = run client actionA
        let! b = run client actionB
        return DBResult.merge a b
        }
    DBAction.Query newAction

// A version of bind taking 2 DBActions arguments
// The applied function takes 2 argument
// - one element from the result list of the first action
// - one element from the result list of the second action
// In this code, we call f on all combination of elements from the lists.
// The newAction function needs to run both actions as it needs to works on their respective DBResults lists.
let bind2 (f: 'a -> 'b -> DBAction<'c>) (actionA: DBAction<'a>) (actionB: DBAction<'b>): DBAction<'c> =
    // define a new function to be wrapped with DBAction.Query
    // CONTINUE
    let newAction (client: DB.DBClient): Async<DBResult.DBResult<'c>> =
        async {
        // run both actions to get their results
        let! a = run<'a> client actionA
        let! b = run<'b> client actionB
        // combine the 2 DBResults
        // in case of error, return the errors encounterd
        // Don't call DBResult.merge in the error case, as it constraints types unecessarily
        // for our case here: DBResult.merge needs both its arguments to hold to same type to be able
        // to build the list in the success case.
        // In our situation here, we don't have this constraint as we use the result of the call to f
        // to build the end DBResult.
        match (a, b) with
        | (Error eA, Error eB) -> return Error(Error.join eA eB)
        | (Error eA, _) -> return Error eA
        | (_, Error eB) -> return Error eB
        // in case of success for both actions, apply the function f on all combinations of elements
        // from both DBResult lists
        | (Ok al, Ok bl) ->
            // We will merge all DBResults we get by applying the function f on pairs of
            // elements of the lists in both DBResults passed as argument
            let (<*>) a b= (async {
                    let! ar= a
                    let! br= b
                    return (DBResult.merge ar br) })
            let rec combineActionsLists listA listB=
                // inner function to combine one element of the first list with every element of
                // the second list
                let rec combineElement a listB =
                    match listB with
                    // we apply function f (returning a DBAction) to both elements and
                    // get its DBResult immediately
                    // Then merge it with the DBResult we get from the recursive call
                    | h::t -> (f a h|> run client) <*> (combineElement a t)
                    // At the end of the list, return a DBResult.zero, holding the empty list
                    // which results in a no-change merge operation.
                    | [] -> async { return DBResult.zero}
                match listA with
                // combine the head of the list with all elements of listB, and merge it
                // with the DBResult we get from the recursive call
                | h::t -> (combineElement  h listB) <*> (combineActionsLists t listB)
                // At the end of the list, return a DBResult.zero, holding the empty list
                // which results in a no-change merge operation.
                | [] -> async { return DBResult.zero}
            return! combineActionsLists  al bl
        }
    DBAction.Query newAction

// combine 2 DBActions with function f
// This appeared useful in the test code where we accumulate the results of
// multiple queries in a record type. In that case, the DBAction returns only
// one "row", the result of a scalar query. The code handles DBActions resulting
// in multiple values returned, but it's not been used nor thought about.
// Note it returns an empty list if one of the 2 actions results in an empty list
// See TestUtils for its use
let combine (f: 'a -> 'b -> 'c) (actionA: DBAction<'a>) (actionB: DBAction<'b>): DBAction<'c> =
    let newAction (client: DB.DBClient): Async<DBResult.DBResult<'c>> = async {
        let! a = run<'a> client actionA
        let! b = run<'b> client actionB
        match (a, b) with
        | (Error eA, Error eB) -> return Error(Error.join eA eB)
        | (Error eA, _) -> return Error eA
        | (_, Error eB) -> return Error eB
        | (Ok al, Ok bl) ->
            return seq {
                for va in al do
                    for vb in bl do
                        yield (f va vb)
            }
            |> Seq.toList
            |> DBResult.DBResult.Ok
    }

    DBAction.Query newAction

// After the triggering action is run successfully (returning an Ok result, even Ok []), call the
// triggeredFunction passing it the result (list) of the triggering action. The triggeredFunction
// must return a DBAction<unit> which will be run immediately.
// The result of the triggeringAction is returned by trigger, making it transparent for the pipeline
// it is inserted in.
// Behaviour is distinct from bind, which will not run if the action returned an empty result.
let trigger(triggeredFunction: 'b list -> DBAction<unit>) (triggeringAction: DBAction<'b>): DBAction<'b> =
    let newAction (client: DB.DBClient): Async<DBResult.DBResult<'b>> = async {
      let! returnedResult = run<'b> client triggeringAction
      match returnedResult with
      | Ok l ->
        do! run client (triggeredFunction l) |> Async.Ignore
      | _ -> ()
      return returnedResult
    }
    DBAction.Query newAction

//******************************************************************************
// Functions to manage savepoints and rollback at the DBACtion level
let setSavePoint (name: string) =
    let newAction (client: DB.DBClient) = client.TrySetSavePoint name
    DBAction.Query newAction

let rollBackToSavePointIfError (name: string) (action: DBAction<'a>) =
    let newAction (client: DB.DBClient) = async {
        let! r1 = run client action
        match r1 with
        | DBResult.DBResult.Ok rows -> return r1
        | DBResult.DBResult.Error es ->
            let! v = client.TryToSavePoint name
            match v with
            | Ok _ ->
                // rollback ok
                // We return the error and all subsequent actions in the DBAction.bind chain will not take place
                return r1
            | Error e ->
                return failwithf "ROLLBACK failed, was the savepoint set?: %s" (Error.toString  e)
    }

    DBAction.Query newAction

// This will rollback the action we get, and returns the possible error, so that subsequent
// actions are not run
let wrapWithSavePointAndRollBackForError (action: DBAction<'a>) =
    let savePoint =
        "DBActionSavePoint"
        + DateTime.Now.Ticks.ToString()

    setSavePoint (savePoint)
    |> bind (fun _ -> action)
    |> rollBackToSavePointIfError (savePoint)
    // FIXME: wrap in transaction if needed, as a savepoint can only be set in a transaction....

// This will rollback the action we get, but will still return a success action so subsequent actions
// are still run
let wrapWithSavePointAndRollBackForErrorButContinue (action: DBAction<DBTypes.ImpactedRows>) =
    wrapWithSavePointAndRollBackForError action
    // Return a success action so subsequent actions run anyway
    // The action is a statement so we return that no row was impacted.
    // FIXME: log the error here
    |> bindError(fun _ -> retn (DBTypes.ImpactedRows 0))

let rollBackIfError (action: DBAction<'a>) =
    let newAction (client: DB.DBClient) = async {
        let! r1 = run client action
        match r1 with
        | DBResult.DBResult.Ok rows ->
            let! () = client.Commit()
            return r1
        | DBResult.DBResult.Error es ->
            let! v = client.TryToFirstSavePointOrRollback()
            match v with
            | Ok _ ->
                // rollback ok
                return r1
            | Error e ->
                printfn "Error causing rollback: %s" (Error.toString  e)
                return failwithf "ROLLBACK failed: %s\nRolled back due to %s" (Error.toString  e)
                    (Error.toString es)
    }

    DBAction.Query newAction

//******************************************************************************
// Function to manipulate the DBAction result

// Ensures the action passed as argument only returns one row.
// Enables the caller to stay at the DBAction level while adding this
// restriction.
let ensureSingleRow (action: DBAction<'a>) =
    let newAction (client: DB.DBClient): Async<DBResult.DBResult<'a>> = async {
        let! v = run<'a> client action
        return (v |> DBResult.ensureSingleRow)
    }

    DBAction.Query newAction

// This function takes the list of result returned by the query, and puts it in a list
// This lets us work on the list as one item (eg when the list holds ids we want to filter on in
// a subsequent DBAction)
let workOnList (action:DBAction<'a>): DBAction<'a list> =
    let newAction (client: DB.DBClient) = async {
        let! r1 = run client action
        match r1 with
        | DBResult.DBResult.Ok rows -> return DBResult.DBResult.Ok [ rows ]
        | Error e -> return Error e
    }
    DBAction.Query newAction

// This function reverses the action of workOnList
let workOnItems (action:DBAction<list<'a>>) : DBAction<'a> =
    let newAction (client: DB.DBClient) = async {
        let! r1 = run client action
        match r1 with
        // one element being a list
        | DBResult.DBResult.Ok [ head::tail ] ->
            return DBResult.DBResult.Ok (head::tail)
        // one element being the empty list
        | DBResult.DBResult.Ok [[]] ->
            return DBResult.DBResult.Ok []
        | DBResult.DBResult.Ok l ->
            return Error ($"wrong call to DBAction.workOnItems, {l} should have been list of one item being a list"
                          |> Error.fromString )
        | Error e -> return Error e
    }
    DBAction.Query newAction

// maps a DBAction<DataTable> returned by dataTableAction to a
// DBAction<string> with a json representation of the results
let mapDataTableToJSON (action: DBAction<DataTable>) =
    let newAction client = async {
        let! v = run client action
        return (v |> DBResult.bind (fun datatable ->
            let json =
                Helpers.dataTableToJSON datatable

            Ok [ json ]))
       }
       // Alternative with Async.map, but seems less approachable
       // return! run client action
       // |> Async.map ( DBResult.bind (fun datatable ->
       //         JsonConvert.SerializeObject(datatable, Formatting.Indented)
       //         |> List.singleton
       //         |> Ok))
       //    }

    DBAction.Query newAction

let mapDataTableToCSV (action: DBAction<DataTable>) =
    let newAction client = async {
        let! v = run client action
        return (v |> DBResult.bind (fun datatable ->
            let csv =
                Helpers.dataTableToCSV datatable

            Ok [ csv ]))
       }

    DBAction.Query newAction

// function to map a list of records to json
// CHECKME: should this replace the function mapDataTableToJSON?
let mapToJSON (action: DBAction<'R>) =
    let newAction client = async {
        let! v = run client (action|> workOnList )
        return (v |> DBResult.bind (fun l ->
            let json =
                Helpers.toJSON l

            Ok [ json ]))
       }
    DBAction.Query newAction

let fromResult<'a,'b> (r:Result<'a,'b>):DBAction<'a> =
    match r with
    | Ok v -> retn v
    | Error e -> error (string e)

let fromOption (o:Option<'a>):DBAction<'a> =
    match o with
      | Some v ->
        retn v
      | None ->
        error("Option was None")

let fromAsync (asyncValue:Async<'T>) =
  let newAction _client = async {
    try
      let! res = asyncValue
      return DBResult.retn res
    with
    | e ->
      return DBResult.errorFromExn e
  }

  DBAction.Query newAction

// start a transaction if we are not in one yet, and wrap
// a bool indicating if we started a new transaction
let beginTransactionIfNeeded () =
    let beginTransationAction (client:DB.DBClient) = async {
        if client.IsInTransaction() then
            return DBResult.retn false
        else
            do! client.BeginTransaction()
            return DBResult.retn true
    }
    DBAction.Query beginTransationAction

// Runs the action passed as argument then :
// - if action is successful, commit if requested
// - otherwise, rollback if commit was requested and return error
let commitTransationIfNeeded (needToCommit:bool ref) (action) =
    let newAction (client:DB.DBClient) = async {
        let! v = run client action
        match v with
        | Ok _ ->
            if needToCommit.Value then
                do! client.Commit()
            return v
        | Error _ ->
            if needToCommit.Value then
                let! _ = client.TryRollback()
                ()
            return v
    }
    DBAction.Query newAction

// Wrap an action in a db transaction if we're not yet in one.
// If we start the tx, it is committed in case of success of action and
// rolled back in case of error. If we don't start the tx, don't commit neither roll back.
let wrapInTransactionIfNeeded (action) =
    // Cell to record if we started a new transaction
    let startedNewTransaction = ref false
    // start transaction if needed and record if we started one in the cell created above
    beginTransactionIfNeeded()
    |> map (fun r -> startedNewTransaction.Value <- r)
    // Now we are sure to be in a transaction, run the action to be wrapped in the transaction
    |> bind (fun _ -> action)
    // After the action ran, commit if needed
    // Note we must pass the ref vell here, as its value is still false when this line is evaluated
    // (because the map call above has not yet run and so couldn't change the cell value yet)
    |> commitTransationIfNeeded startedNewTransaction

//******************************************************************************
// Helper module holding builders to create Query and Statement instances

module StaticArguments =
    // We define an interface to avoid problem in the type inference
    // It is problematic from an boxed object of type (Static<'a> v) to
    // get the value v or type 'a .In the use below, the inference always led to
    // the type Static<obj> when the value was unboxed.
    // Defining an interface implemented in the type allows to get the value of the
    // right type
    // For more info, see existential  types:
    // https://stackoverflow.com/a/5520212
    // https://www.gresearch.co.uk/article/squeezing-more-out-of-the-f-type-system-introducing-crates/
    type IStatic =
        abstract AsString : string
    // The type used to wrap query argument that should be substituted as static, i.e
    // that should not be passed as a postgresql query argument (which are escaped to
    // avoid SQL injection)
    type Static<'a> = Static of v:'a with
        static member Get<'a>(Static v) : 'a=  v
        interface IStatic with
                override this.AsString = (Static.Get(this).ToString())


// Module holding helper functions to write DBAction builder functions
// See comment below about the functions using these helpers, it explains why
// some helpers that seem replaceable by composition of other existing functions
// are still defined
module Helpers =
    open StaticArguments
    /// Convert printf format place holders in a printf format string to sequencially numbered @p1, @p2, ... placeholders
    /// Initially for Dapper, left as is
    let convertPlaceholders format =
        // This is the last parameter index handled.
        // As the first parameter must have index 0, initialise it to -1
        // This is a cell so the evaluator can update the index
        // https://docs.microsoft.com/en-us/dotnet/api/system.text.regularexpressions.regex.replace?view=netframework-4.7.2#System_Text_RegularExpressions_Regex_Replace_System_String_System_String_System_Text_RegularExpressions_MatchEvaluator_
        let placeHolderIndex = ref -1

        // The evaluator passed to Regexp.Replace that will return the replacement for the match.
        let placeHolder (m: Match) =
            // update index for this match
            incr placeHolderIndex
            // return the new placeholder string
            sprintf "@p%d" !placeHolderIndex

        Regex.Replace(format, "%.", placeHolder)

    // Replace staticArgs placeholders by their value, taken from the values list
    // This is useful for example if the table name is passed as a parameter, which is not accepted by postgresql
    let replacePlaceholders  (format: string) (values: obj list) =
        // This is the last parameter index handled.
        // As the first parameter must have index 0, initialise it to -1
        // This is a cell so the evaluator can update the index
        // https://docs.microsoft.com/en-us/dotnet/api/system.text.regularexpressions.regex.replace?view=netframework-4.7.2#System_Text_RegularExpressions_Regex_Replace_System_String_System_String_System_Text_RegularExpressions_MatchEvaluator_
        let placeHolderIndex = ref -1
        let replacementsCount = ref 0
        // list of values that where not replaced as static argument
        // we will return it so the values correspondint to the untouched markers are available
        let newValues = ref []
        // helper function
        let keepValue (o:obj) =
            newValues.Value <- List.append newValues.Value [o]

        // The evaluator passed to Regexp.Replace that will return the replacement for the match.
        let placeHolder (m: Match) =
            // update index for this match
            incr placeHolderIndex

            let currentValue = (unbox(List.item !placeHolderIndex values))
            // We only statically replace %A markers
            if m.Value = "%A" then
                let typ = currentValue.GetType()
                // and only if the value passed is of our DU type Static
                if (FSharpType.IsUnion(typ) )then
                    let cases = FSharpType.GetUnionCases(typ)
                    if (Array.length cases = 1) && ((cases.[0]).Name = "Static" ) then
                        incr replacementsCount
                        // We unbox and retrieve the individual value from the list here to access is as an
                        // IStatic implementation, allowing us to get the string representation of the wrapped
                        // value.
                        let workValue:IStatic = (unbox(List.item !placeHolderIndex values))
                        // As this is a placeholder that has to be replaced in the query before handling it
                        // to postgres, we just replace it by the corresponding value
                        workValue.AsString
                    else
                        keepValue currentValue
                        // leave the marker as is
                        m.Value
                else
                    keepValue currentValue
                    // leave the marker as is
                    m.Value
            else
                keepValue currentValue
                // leave the marker as is
                m.Value

        // we match on all markers and only work on %A with Static values
        // we need to match all markers to keep values of the markers we don't
        // statically replace
        let reg = Regex("%.")
        let updateFormat = reg.Replace(format, placeHolder)
        updateFormat, newValues.Value

    // The parameters for our query functions need to be placed in a dictionary.
    let buildParametersMap (values: obj list) =
        //let expando = System.Dynamic.ExpandoObject()
        //let expandoDictionary = expando :> IDictionary<string,obj>
        //// put values in dictionary under sequential keys p0, p1, ... so that the correspondance
        //// with their respective placeholders is maintained
        //List.iteri (fun i v -> expandoDictionary.Add(sprintf "p%d" i, v)) values
        //expandoDictionary
        // map version
        let m = new Map<string, obj>([])
        // map requires to use fold as a new map is returned by Add
        let (r, _) =
            List.fold (fun (acc: Map<string, obj>, i) v -> (acc.Add(sprintf "p%d" i, v), i + 1)) (m, 0) values

        r

    // <summary>This function replaces the static arguments in a query format string</summary>
    // <returns>The pair (updatedFormatString, updatedValuesList)</returns>
    let handleStaticArguments (format: string) (values: obj list) =
            // replace static args in format string
            let queryFormat, newValues =
                replacePlaceholders  format values
            /// convert placeholder from printf format (%{type}) to Dapper format (@p{index})
            let query = convertPlaceholders queryFormat
            // build parameters, dropping those that are static
            let parametersDict = buildParametersMap newValues
            query,parametersDict
    // This callback will return a QueryAction.
    // It just renames placholders in the query string, and put parameters in a dictionary
    // before passing it to the client.
    // Issues a statement and returns a reader
    let dataTableActionBuilder (format: string) (types: System.Type list) (values: obj list) =
        let action (client: DB.DBClient) =
            let query,parameters = handleStaticArguments  format values
            // FIXME uniformise MAP/Dictionary usage
            client.qDataTable (query, parameters)

        DBAction.Query action

    // This callback will return a Statement action.
    // It just renames placholders in the query string, and put parameters in a dictionary
    // before passing it to the client.
    // Issues a statement and returns The DBResult<int> holding the number of rows impacted
    let statementActionBuilder (format: string) (types: System.Type list) (values: obj list) =
        let action (client: DB.DBClient) =
            // call added during preparation of static args support.
            // as it has 0 static args, the format string is not changed, but the
            // parameters dictionary is still built as required.
            let query,parameters = handleStaticArguments format values
            // convert placeholder from printf format (%{type}) to Npgsql format (@p{index})
            // FIXME uniformise MAP/Dictionary usage
            client.executeStatement query parameters

        DBAction.Statement action

    // This callback will return a QueryAction.
    // We call static arguments those that need to be replaced in the query before handling it
    // over to postgres (eg the table name).
    // After that, it just renames placeholders in the query string, and put parameters in a dictionary
    // before passing it to the client.
    let actionQueryBuilder
        (format: string)
        (types: System.Type list)
        (values: obj list)
        =
        let action (client: DB.DBClient) =

            let query,parametersDict = handleStaticArguments format values
            client.q
                (query,
                 parametersDict
                 |> Seq.map (|KeyValue|)
                 |> Map.ofSeq)

        DBAction.Query action

    /// Callback to build a single row returning DBAction, resulting in an error if more rows are returned by the query
    let singleRowQueryBuilder
        (format: string)
        (types: System.Type list)
        (values: obj list)
        =
        actionQueryBuilder format types values
        |> ensureSingleRow

    // returns a DBAction.Query action because, although it inserts date in the
    // database, it does not return the number of impacted rows, but the id of
    // the inserted row.
    // That action will insert one row and return the id of the newly inserted
    // row under the type 'a For that to work, the insert statement needs to
    // include the postgres-specific `RETURNING id` part
    // Initially, this function had a type argument, because I thought it was
    // needed to have access to that type as this function calls `typeof` on it.
    // However, this prevents the function to be fully generic, causing trouble
    // when the function is called in another file (see
    // https://github.com/dotnet/fsharp/issues/8434). In this case, the type
    // needed inside the function is a type argument of the return type, so
    // rather than adding type arguments to the function, it is possible to
    // achieve the same result by explicitly specifying the return type
    // `DBAction<'a>`. Type inference will set determine the type `'a` and make
    // it available in the function to call `typeof<'a>`.
    let actionSingleInsertBuilder
        (format: string)
        (types: System.Type list)
        (values: obj list)
        :DBAction<'a>
        =
        // First define local functions used in an Option.bind chain
        //----------------------------------------------------------

        // DBAction that will be returned
        //-------------------------------
        let action (client: DB.DBClient) = async {
            let query,parameters = handleStaticArguments format values
            // execute the statement, and handle the returned value, which is the id of the row
            // converting to a SingleResult ensure we have only one row, and simplifies the pattern match below
            let! result_1 =
                client.qDataTable
                    (query,
                     parameters
                     |> Seq.map (|KeyValue|)
                     |> Map.ofSeq)
            let result = result_1 |> DBResult.toSingleResult

            match result with
            // if the number of rows is greater than 0, it is necessarily 1, as we call toSingleResult before.
            // When we get an ok result with just one row, all seems ok and we can extract id of the row
            // We load the data returned in a datatable, which will have one row and one column named "id"
            | Ok t when t.Rows.Count > 0 ->
                let rowId = (t.Rows.Item 0).["id"]
                // we want to wrap it in a one case discriminated union type.
                // that type was passed a type argument, and we need to get its type
                let idType = typeof<'a>
                // get and array of disciminated union cases
                let duCases =
                  try
                          // pass true as last argument so it also uses private constructors
                          Reflection.FSharpType.GetUnionCases(idType,true)
                  with
                  | e ->
                      MyOwnDBLib.Logging.Log.Error(
                          $"Query {query} tried to get DU cases but failed with {e}"
                      )
                      raise (Exception($"Query {query} tried to get DU cases but failed with {e}"))
                // instanciate the Union case and get an option
                let idUnionCase =
                    format
                    |> DBHelpers.tableIdUnionCaseName
                    |> Option.bind (DBHelpers.findUnionCaseIn duCases)
                    |> Option.bind DBHelpers.isIdName
                    // pass true as last argument so it also uses private constructors
                    |> Option.bind (fun case -> Some(FSharpValue.MakeUnion(case, [| rowId |], true) :?> 'a))
                // return a DBResult
                match idUnionCase with
                | Some uc -> return DBResult.DBResult.Ok [ uc ]
                | None ->
                    return (
                        (sprintf "Wrapping type for row id (%A) not found, this is not right. Was looking for %A" idType (DBHelpers.tableIdUnionCaseName format))
                        |> DBResult.error
                    )
            // empty result
            | Ok t ->
                return (DBResult.error
                            (sprintf
                                "empty result, but required (failed update? forgot to include 'RETURNING id' in query?), for query %s with parameters %A"
                                 format values))
            | Error e -> return (DBResult.DBResult.Error e)
        }

        DBAction.Query action


// Here are functions using the helper functions.
// They need to call Utils.formatStringHandler explicitly, to ensure that the
// format string is handled correctly. This can be understood intuitively as the
// format string will require additional arguments that will be passed in. If
// the function doesn't have the Utils.formatStringHadnler call as last
// expression, those additional arguments won't be seen by the calling function,
// leading to errors at the call site of the kind "FS0003: This value is not a
// function and cannot be applied."
// The downside is that additional helper functions need to be defined for specific behaviours.
// For example, the  querySingleAction function could theorically be of the form
//    queryAction format |> ensureSingleRow
// but then the Utils.formatStringHandler can't do its things regarding the
// format string parameters. That's why this code is replace by
//    Utils.formatStringHandler Helpers.singleRowQueryBuilder format
// which requires the definition of a new helper.

//******************************************************************************
// Functions instanciating a DBAction and running it immediately.

// We call static arguments those that need to be replaced in the query before handling it
// over to postgres (eg the table name). The number of static arguments is the first parameter we get.
let immediate(client: DB.DBClient) format =
    let immediateCallback query types values =
        (Helpers.actionQueryBuilder query types values)
        |> run client

    Monazita.Utils.formatStringHandler immediateCallback format


//******************************************************************************
// Query creation functions
// Query return information from the database's content in
// -  a base datatype (string, int, etc)
// - an anonymous record
// - a model record, with mappings to Id types and Option types

// Build a query action. Accepts a printf format string and its arguments.
// Can also issue queries without arguments if the query is passed as a string literal.
// For parameterless queries passed as a variable, staticQueryAction must be
// used. This is because a string variable is not of of the type PrintfFormat,
// whereas the literal string will be seen by the compiler as a PrintfFormat.
let queryAction format =
    // We need to pass a callback to the function handling the PrintfFormat to get
    // the format string and all info about the values passed to it
    Monazita.Utils.formatStringHandler Helpers.actionQueryBuilder format
let staticQueryAction (query:string) =
    // We need to pass a callback to the function handling the PrintfFormat to get
    // the format string and all info about the values passed to it
    Helpers.actionQueryBuilder query [] []

/// build a single-row returning DBAction.Query with the parameterised sql query and its parameters
/// Returns a single element list of scalar or record, according to the type inferred
/// IMPROVE: make it return the inferred type (esp forId types) like singleInsertAction
let querySingleAction format =
    Monazita.Utils.formatStringHandler Helpers.singleRowQueryBuilder format

//******************************************************************************
// Actions returning their results in a DataTable instance

// Build a DBAction.Statement from the parameterised query and the record.
// Can handle a parameterless query in a literal string, but if the query is
// stored in a variable, use staticDataTableAction.
let dataTableAction format =
    Monazita.Utils.formatStringHandler (Helpers.dataTableActionBuilder) format

let staticDataTableAction format =
    Helpers.dataTableActionBuilder  format [] []

//******************************************************************************
// Single insert actions, returning the id of the inserted row
// Do not specify type argument of the singleInsert* functions, as this will prevent
// it to be fully generic.
// See comment for actionSingleInsertBuilder for further details.
// The inferred return type MUST be a single case DU, as it is supposed to return the
// id of the row inserted.

let inline singleInsertAction (format) =
    Monazita.Utils.formatStringHandler (Helpers.actionSingleInsertBuilder) format

//******************************************************************************
// Statement creation functions
// Returns the number of rows impacted by the statement execution, wrapped in DBTypes.ImpactedRows

// Build a DBAction.Statement with a PrintfFormat argument
let statementAction format=
    Monazita.Utils.formatStringHandler Helpers.statementActionBuilder format

/// Build a DBAction.Statement from the parameterised query and the record.
let recordStatementAction (query: string) (record: 'Record) =
    let action (client: DB.DBClient) =
        client.executeStatementOnRecord (query, record)

    DBAction.Statement action
