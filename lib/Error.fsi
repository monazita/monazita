module Error

type t

type Errors = t


val toString: Errors -> string
val toStringList: Errors -> string list
val toShortStringList: Errors -> string list
val fromString: string -> t
val fromExn: exn -> t
val join: Errors -> Errors -> Errors
val numbers: Errors -> int
val getExceptions: Errors -> exn list
