module Helpers
    open Newtonsoft.Json
    let resultOfOption (logFunction: unit -> unit) (error: string) (idOption: 'a option) =
        match idOption with
        | None ->
            logFunction ()
            Error [ error ]
        | Some id -> Ok id

    // Converts a Pair of Results to a Result of a Pair
    // See https://fsharpforfunandprofit.com/posts/elevated-world-4/#the-sequence-function
    let tupleSequenceToResult (r1, r2) =
        r1
        |> Result.bind (fun v1 ->
            r2
            |> Result.map (fun v2 ->
                v1, v2)
                )

    open SerialisableId
    open System
    open Newtonsoft.Json.Linq
    // Our custom JSON serialiser
    type IdSerializer() =
        inherit JsonConverter()
        // This serialiser only converts objects implementing the interface SerialisableId.
        override self.CanConvert(objectType:Type)=
            objectType.GetInterface("SerialisableId",false)
            |> function
                | null -> false
                | _ -> true
        // write the JSON for the value
        // The generated object ot be place in the JSON output is of the form
        // {
        //     "Case": "EntityId",
        //     "Fields": [
        //         11
        //     ]
        // }
        override self.WriteJson(writer:JsonWriter, value:obj, serializer: JsonSerializer) =
            // we get an instance of SerialisableId, which has the method getJSONInfo() giving us
            // the name of the constructor and the value wrapped
            let info :{|name:string; value:int|} = ((unbox value) :> SerialisableId).getJSONInfo()
            // The generated JSON is an object with fields "Case" (string) and "Fields" (array of ints, of length 1)
            // Construct that object
            let o = JObject()
            o.AddFirst(new JProperty("Case", info.name))
            o.Add(new JProperty("Fields", [info.value]))
            // Write the object to the output
            o.WriteTo(writer)
        override self.ReadJson(writer:JsonReader, objectType:Type, value:obj, serializer: JsonSerializer) =
            failwith "Not a reader, not implemented"
        override self.CanRead = false

    // FIXME: check if serialising to json is fine here
    // https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/dataset-datatable-dataview/security-guidance
    let dataTableToJSON (dt:System.Data.DataTable) =
        JsonConvert.SerializeObject(dt, Formatting.Indented)

    let dataTableToCSV (table:System.Data.DataTable) =
        let s = ref [||]
        if table.Columns.Count = 0 then
            s := Array.append !s [| "" |]
        else
            // For both headers and content, we get each row's value in an array of string
            // which we then strong.concat, avoiding a trailing comma.
            // Columns are hard to get in a seq, hence this approach
            let headers = ref [||]
            // collect columns headers in the headers array
            for c in table.Columns do
                let header = sprintf "\"%s\"" (c.ColumnName.Replace("\"","\"\""))
                headers := Array.append !headers [| header |]
            let headersRow = (!headers) |> String.concat(",")
            // Add line ending
            s := Array.append !s [| headersRow;|]

            // one row can be obtained as an array, facilitating the manipulation
            for r in table.Rows do
                let csvRow = r.ItemArray
                                |> Array.map (fun v -> v.ToString().Replace("\"","\"\""))
                                |> Array.map (fun s -> String.Concat [ "\""; s; "\""])
                                |> String.concat ","
                // Add line ending
                s := Array.append !s [| "\n"; csvRow |]
        String.concat "" !s


    let toJSON (dt:List<_>) =
        JsonConvert.SerializeObject(dt, Formatting.Indented, IdSerializer() )