namespace Monazita

module Utils=
    // For System.Type
    open System
    // For FsharpType
    open Microsoft.FSharp.Reflection
    open Newtonsoft.Json


    /// <summary>Function handling the printf format strings. It returns a function that will consume the arguments required
    /// by the format strings.</summary>
    /// <param name="callback">All information extracted from the format string and its arguments are passed to this callback.
    /// This is:
    /// - the format string
    /// - the types of the values passed to the format string
    /// - the boxed values passed to the format string</param>
    /// <param name="format">The format string.</param>
    /// <returns>A function that will consume the values required by the format string, and then call the callback.</returns>
    /// <example>let myTranslator = Utils.formatStringHandler translationCallback format</example>
    let formatStringHandler (callback: string -> Type list -> obj list -> 'ReturnType)
        (format: PrintfFormat<'PrintfType, _, _, 'ReturnType>): 'PrintfType =
        /// This function will be called recursively until all arguments of the format string have been provided.
        /// Each call consumes one argument of the format string.
        /// When the last argument is received, the callback is called.
        /// It receives as arguments:
        /// - its own type (domain -> range)
        /// - the types list of the arguments handled by its parent calls (it is an accumulator)
        /// - the values list of the arguments handled by its parent calls (it is an accumulator)
        /// - the format string argument it will handle itself.
        /// All but the last argument are set by partial application, and are bookkeeping values.
        let rec consumer (myType: Type) (typesList: Type list) (valuesList: obj list) (v: obj) =
            // Extract our domain to add it the the typeList accumulator, and the range
            // to build the next recursive call signature (if the range is a function signature)
            let domain, range = FSharpType.GetFunctionElements myType
            // accumulate types and lists
            let nextTypesList = (domain :: typesList)
            let nextValuesList = (v :: valuesList)
            // if our range is a function, we return a partial application of ourself, on which
            // we set the type signature (which is indeed our range).
            if FSharpType.IsFunction range then
                let internalConsumer = consumer range nextTypesList nextValuesList
                // MakeFunction returns an object, no need to box here
                FSharpValue.MakeFunction(range, internalConsumer)
            else
                // pass full lists to our handler
                // note that values have the right type in the handler
                // box the return value to have an object
                box (callback (format.Value) (List.rev nextTypesList) (List.rev nextValuesList))
        // Initialise the consumer of the first value required by the format string with a partial
        // application of consumer, the accumulators being the empty list.
        let argsConsumer = consumer typeof<'PrintfType> [] []
        // Check if we need to return a function (there is a placeholder to handle)
        // or not (there is no placeholder in the format string)
        if FSharpType.IsFunction typeof<'PrintfType> then
            // if our type is of a function, construct it and return it
            // MakeFunction returns an obj. We unbox it to have instance of PrintfType,
            // which is the return type we explicitly set above
            unbox (FSharpValue.MakeFunction(typeof<'PrintfType>, argsConsumer))
        else
            // if our type is not a function, it means there is no placeholder in the
            // format string. Directly call the callback, passing empty lists for types and values
            unbox (callback format.Value [] [])

    let reformatJSON(json:string) =
        JsonConvert.SerializeObject(JsonConvert.DeserializeObject(json), Formatting.Indented)

    let toJson (o:obj) = JsonConvert.SerializeObject(o)

    // get the PrintFormat instance corresponding to the string passed as argument.
    // The returned value can be used to consume the arguments present in the string.
    // To keep it fully generic, as needed to be able to call it from another file, it
    // has 2 type arguments that can usually be left over to inference, and specified as _:
    // - the function type of the PrintfFormat function that will handle its arguments.
    // - the type of the value returned by the callback function (see formatStringHandler)
    //   when all arguments have been consumed.
    let getPrintfFormat<'F,'R> s :PrintfFormat<'F,unit,unit,'R>  = s
