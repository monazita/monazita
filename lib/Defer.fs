module Defer

open System
// For Ply CE builders
open FSharp.Control.Tasks

// Creates an instance of IDisposable, that the caller as to assign with `use`
// Usage example:
//      use _disposable = Defer.defer (fun () -> printfn "exiting scope")
// Imlpementing go's defer is not possible with quotations, as the evaluated
// quotation whould have to be inserted at the call site. This achieves the same
// though
let defer (f: unit -> unit) =
    {new IDisposable with member _.Dispose() = f ()}

let deferAsync (f: unit -> Async<unit>) =
    {new IAsyncDisposable with member _.DisposeAsync() = unitVtask { return! (f ())} }
