namespace MyOwnDBLib

open Serilog
// For logging level switch
open Serilog.Core
// For LogEventLevel
open Serilog.Events

// The app using the lib initialises logging by calling either setDefaultLogger()
// or setLogger(logger:ILogger)
// In tests, no logging is done before setDefaultLogger() is called. This is not
// such a big deal as logs will probably be needed only when debugging a test, and
// in that case the logging can be enabled in that test itself.
// So to add lib log collection in a test:
// - call MyOwnDBLib.Logging.useDefaultLogger()
// - possibly change the level with MyOwnDBLib.Logging.setDefaultLoggerLevel(MyOwnDBLib.Logging.Verbose)
// To add logs generation in the lib:
// - open MyOwnDBLib.Logging
// - issue a log message with eg Log.Verbose

module Logging=
    // make our level switching type safe
    type Level = |Verbose |Debug |Information |Warning |Error |Fatal
    // Set the Serilog level switcher
    // The default logger will mainly be used from tests, so let's make the
    // default logger level high enough to be immediately useful
    let mutable private levelSwitch = LoggingLevelSwitch(LogEventLevel.Debug);

    // Defining this type alias lets all code in the same namespace or opening our namespace
    // to do logging simply by calling eg Log.Verbose. This will allow to possibly extract
    // code from MyOwnDBLib to specific libs, eg put the DBAction reader monad in a lib.
    let mutable Log = Serilog.Log.Logger
    // Remember this is probably under ./bin/Debug/net6.0/
    let mutable private logPath = "logs/lib.log"
    //  Function returning our default logger
    let private getDefaultLogger () =
        Serilog.LoggerConfiguration()
            .MinimumLevel.ControlledBy(levelSwitch)
            .Destructure.FSharpTypes()
            .WriteTo.File( path=logPath, rollingInterval=RollingInterval.Day)
            .Enrich.FromLogContext()
            .CreateLogger();
    // Modifies the log path for our default logger. When changed, setDefaultLogger() must
    // be called afterwards.
    let setDefaultLoggerPath (path:string) =
        logPath <- path
    // Sets the default logger.
    let useDefaultLogger () =
        Log <- getDefaultLogger()
    // Change the log level of the default logger
    let setDefaultLoggerLevel (l:Level)=
        match l with
        | Verbose -> levelSwitch.MinimumLevel<- LogEventLevel.Verbose
        | Debug -> levelSwitch.MinimumLevel <- LogEventLevel.Debug
        | Information -> levelSwitch.MinimumLevel <- LogEventLevel.Information
        | Warning -> levelSwitch.MinimumLevel<- LogEventLevel.Warning
        | Error -> levelSwitch.MinimumLevel <- LogEventLevel.Error
        | Fatal -> levelSwitch.MinimumLevel <- LogEventLevel.Fatal
    // If the app using this library already has a ILogger, it can be used
    // by this lib too.
    let setLogger (logger:ILogger) =
        Log <- logger