module Error

open System.IO

type AppStr =
    | S of string
    | E of exn
// Need to use a single discriminated union here as the F@ compiler cannot
// hide plain types: https://stackoverflow.com/questions/2855735/f-cant-hide-a-type-abbreviation-in-a-signature-why-not
type t = Internal of AppStr list

type Errors = t

let toStringList (Internal es: t) =
    let folder el acc =
        match el with
        | S s -> s :: acc
        | E e -> (e.ToString()) :: acc
    // use foldBack or the list is reversed
    List.foldBack folder es []

// Same as toStringList, but for exception only return message
// Useful in test code to validate exception raised
let toShortStringList (Internal es: t) =
    let folder el acc =
        match el with
        | S s -> s :: acc
        | E e -> (e.Message) :: acc
    // use foldBack or the list is reversed
    List.foldBack folder es []

let toString  (Internal es: t) =
    Internal es
    |> toStringList
    |> String.concat "\n"

let fromString (s: string) = Internal [ S s ]
let fromExn (e: exn) = Internal [ E(e) ]

let join (Internal es1) (Internal es2) = Internal(List.concat [ es1; es2 ])
let numbers (Internal es) = List.length es

// return only exceptions from the errors list
let getExceptions (Internal es) =
    es
    |> List.choose (function
                    | E e -> Some e
                    | _ -> None)