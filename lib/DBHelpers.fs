module DBHelpers

// for record instanciation
open Microsoft.FSharp.Reflection
// for PropertyInfo
open System.Reflection
open System.Text.RegularExpressions
open Humanizer

open System.Data.Common

open Npgsql

// for DataTable
open System.Data

open DBTypes

// Check we have a id type (these are in the module Ids)
let isIdType (t:PropertyInfo) =
    let r = Regex.IsMatch(t.PropertyType.FullName,"Id+.*")
//    if r then
//        printfn "we have full type name %s considered as id" t.PropertyType.FullName
//    else
//        printfn "we have full type name %s NOT considered as id" t.PropertyType.FullName
    r

        // build the union type case we should use based on the table name
        // This will map date_detail_values to DateDetailValueId
let buildIdTypeNameFromTableName (tableName:string) =
    // As we don't know if the table name is plural, pass the argument to Singularize,
    // otherwise it returns null
    tableName.Singularize(inputIsKnownToBePlural= false).Pascalize() + "Id"

// Extracts the table name from the query, and uses it to build the table's Id type.
// This is to automatically find the id type 'UserId' from the table name 'users'.
let tableIdUnionCaseName (statement: string) =
    let reg =
        Regex("(insert into|update|select .* from) (?<tableName>\w+)")

    let m = reg.Match(statement)
    if m.Success then
        let case = buildIdTypeNameFromTableName (m.Groups.["tableName"].Value)
        Some case
    else
        None
// function validating the name of the union case is formatted as expected
let isIdName (i: UnionCaseInfo) =
    if Regex(".*Id").Match(i.Name).Success then Some i else None
// Returns the DU case (of type UnionCaseInfo) with the searchedName
let findUnionCaseIn (duCases: UnionCaseInfo array) (searchedName: string) =
    match duCases.Length with
    | 0 -> None
    | _ ->
        duCases
        // Case insensitive filtering was added for handling value less DU constructors (eg type Protocol=|Smtp |Http)
        |> Array.tryFind (fun i -> i.Name.ToLower() = searchedName.ToLower())
// This is used when putting a query result in a record, and
// is used to map values retrieve from the database to the
// fields of the record type we defined for the table
// We get a tuple giving info about the record's field:
// - type
// - is it a union type?
// - is it an id type?
// - the name of the record's field
// as well as the value for that field.
// We return the boxed value for that record field, handling union type and null
// values.
// We need to box the returned value because
// 1) we would otherwise return different types in the if branches
// 2) the MakeRecord function takes an object array for the values, and this
// function returns the values of the arrays passed to MakeRecord
let buildIdTypeValue typ (idCase:string option) v =
    let cases =Reflection.FSharpType.GetUnionCases(typ,true)
    match Array.length cases with
    // if not found, we cannot continue
    | 0 -> failwithf "DU case for id colum type (%A) not found" typ
    // if only case in DU type, use it
    | 1 -> FSharpValue.MakeUnion(cases.[0], [| v |], true)
    // if multiple cases for the type try to identify the case based on
    // the table name, otherwise fail
    | _ ->
        let idFromTableName =
            idCase
            |> Option.bind (findUnionCaseIn cases)
            |> Option.bind isIdName
            |> Option.map (fun c -> FSharpValue.MakeUnion(c, [|v|],true))
        match idFromTableName with
        | Some v -> v
        | None -> failwithf "id type case not found for %A" typ

// This will build a value for a valueless DU type, eg type Protocol = |Smtp |Http
// It lets us store the string value in the database but still build the stronly typed
// value in our code
// typ is the type of the record field we need to return
// v is the value found in the database, which must correspond to a DU case
let buildDUTypeValue typ (v:string) =
  let cases = Reflection.FSharpType.GetUnionCases(typ,true)
  match Array.length cases with
  // if not found, we cannot continue
  | 0 -> failwithf "DU case for colum type (%A) not found" typ
  // if multiple cases for the type try to identify the case based on
  // the table name, otherwise fail
  | _ ->
      let duCaseFromValue =
          v
          |> findUnionCaseIn cases
          |> Option.map (fun c -> FSharpValue.MakeUnion(c, [||],true))
      match duCaseFromValue with
      | Some v -> v
      | None -> failwithf "DU case not found for %A, trying to map value %s" typ v

let attemptBuildDUTypeValue typ v =
    // unbox to cjeck we have a string
    let valueTyp = (unbox v).GetType()
    if valueTyp = typeof<string> then
      let unboxedValue = unbox v
      buildDUTypeValue typ unboxedValue
    else
      failwithf "Trying to map to a valueless DU case, but the value we got to map to a case was not a string but a %A: %A" valueTyp (unbox v)

// Wrap the value in a DU case if needed, ie if it is an id, or if the type to map to is an enum (in which case we map to valueless DU cases)
// idCase is the case we want to wrap the value in if it is an id type
let wrapValueIfNeeded isid (typ:System.Type) (idCase) v =
    // First check if we need to map to an Id type
    if isid then
      buildIdTypeValue typ idCase v
    // Then check if we need to map to a valueless DU case.
    // This currently maps a string to a similarly named case
    else if FSharpType.IsUnion(typ,true) then
      attemptBuildDUTypeValue typ v
    // If not mapped to a union case, we use the value as is
    else
      v

open FSharp.Data
// idCaseFromTableName is the case to wrap the value with if this is an id type.
let toBoxedValue ((typ:System.Type,isunion:bool,isid:bool,name:string,idCaseFromTableName:string option),v:obj) =
    // The JsonValue of a union type, but we want to handle distincly, see in the else
    if isunion && (not (typ.IsEquivalentTo(typeof<JsonValue>))) then
            // Handle fields that are Option
            if typ.IsGenericType && typ.GetGenericTypeDefinition() = typedefof<Option<_>> then
                // We have an option type wrapping a Id type
                // Extract the type wrapped by the option
                let wrappedType = typ.GetGenericArguments()[0]
                // Extract individual cases
                match ((unbox v).GetType()) = typeof<System.DBNull> with
                | true ->
                    // If the value is null, we return none
                    let none = typ.GetMethod("None")
                    box none
                | _ ->
                    // If the value is non-null, construct a Some wrapping it
                    // Build a instance of the wrapped type
                    let idValue = wrapValueIfNeeded isid wrappedType (Some wrappedType.Name) v
                    // Now build the Option value
                    let Som = typ.GetMethod("Some")
                    let v2 = Som.Invoke(null,[|idValue|])
                    box v2
            else
              wrapValueIfNeeded isid typ idCaseFromTableName v
    else
        // we don't handle the case where a non union type is of type DBNull
        if ((unbox v).GetType()) = typeof<System.DBNull> then
            let msg = sprintf "%s of type %A has a null value but its field is not an option" name typ
            failwith msg
        // simply box the non union type value
        else
          // If the record field is of type JsonValue, parse it here.
          // When the caller has loaded the FSharp.Data.JsonExtensions, the
          // json value can be accessed like r.jsonvalue?field.AsString()
          if typ.IsEquivalentTo(typeof<JsonValue>) then
            let jsonValue =JsonValue.Parse (unbox v)
            box jsonValue
          else
            v

// Instanciate one record of type 'Rec with data provided by the NpgsqlDataReader
// The names of the fields in both the record and the table must match.
// We must get the cmd argument to determine the ID type's DU case to use if the id type
// has more than one case (eg for DetailValueIdType with DateDetailValue, DdlDetailValue,...)
let instanciateRecord<'Rec> (cmd:NpgsqlCommand) (read:DbDataReader) : 'Rec=
    let recordType = typeof<'Rec>
    let recordFields = FSharpType.GetRecordFields recordType

    let query = cmd.CommandText
    // See if we can extract an Id type name from the query's table name
    let idName = tableIdUnionCaseName query

    // collect info about the record fields in a tuple with fields
    // - type
    // - boolean indicating if it is a union type
    // - bootlean indicating if it is an Id type we define in module Ids
    // - name of the field
    // - IdName: case of the Id DU to instanciate the Id with, ed DateDetailValueId
    let fieldsInfo = recordFields
                    |> Array.map (fun t ->
                                        (t.PropertyType,
                                        // true as last args to also look at private constructors
                                         FSharpType.IsUnion(t.PropertyType, true),
                                         isIdType t,
                                         t.Name,
                                         idName
                                        )
                        )
    // collect values to be placed in the records
    let values = recordFields
                 // read the field values from the NpgsqlDataReader
                 |> Array.map (fun info ->read.get_Item info.Name )
                 // then build a tuple with the List.zip function
                 |> Array.zip fieldsInfo
                 // and map this tuple to a values array
                 |> Array.map toBoxedValue

    FSharpValue.MakeRecord(recordType,values,true) :?> 'Rec

// This is used when inserting data in the database, and maps
// record values to types understood by postgresql.
// It handles null values and options
let recordFieldToPg (b:obj) =
    let v = unbox b
    if isNull v then
        box (System.DBNull.Value)
    else
        // We handle the option type
        if FSharpType.IsUnion (v.GetType()) then
            let (v1,v2) = FSharpValue.GetUnionFields(v,(v.GetType()), true)
            if v1.Name = "None" then
                box (System.DBNull.Value)
            else
                if Array.length v2 > 1 || v1.Name <> "Some" then
                    let msg = sprintf "This is probably a union type we don't handle: constructor: %s with %d values"  v1.Name (Array.length v2)
                    failwith msg
                // if it is Some val, we can take the first element of the array values
                v2.[0]
        else
            box v

// Functions executing NpgsqlCommands are grouped in the module QueryHandlers.
// These functions are called by a queryWrapper function, that does some work
// before and after the call (opening connection if needed, handling
// transactions, catching exceptions, ...)
module QueryHandlers =
    // Puts the results of the query in a list of records of type 'Rec
    // 'Rec can be passed as anonymous record
    let recordQueryHandler<'Rec> (cmd:NpgsqlCommand):Async<DBResult.DBResult<'Rec>> =
        if FSharpType.IsRecord typeof<'Rec> then
            // use the reader so that it is closed when exiting
            // failing to close it will cause the error
            // Npgsql.NpgsqlOperationInProgressException: A command is already in progress
            async {
            use! reader = cmd.ExecuteReaderAsync() |> Async.AwaitTask
            return Ok [ while reader.Read() do
                            yield instanciateRecord<'Rec> cmd reader
            ]
            }
        else
            // IMPROVE: better error reporting, eg with query causing error?
            async {
            use! reader = cmd.ExecuteReaderAsync() |> Async.AwaitTask
            if reader.VisibleFieldCount >1 then
                return DBResult.error (sprintf "Multiple columns returned by query when returning scalar, use a record to store result instead.\nIf your type is a record, be sure it is reachable (not private, present in the .fsi).\nCommand was: %s" cmd.CommandText)
            else
                return Ok [ while reader.Read() do
                                yield unbox (reader.get_Item 0 )
                ]
            }

    // Puts data read in a datatable.
    // More flexible than putting data in a record, expecially for Crosstab results.
    let datatableQueryHandler (cmd:NpgsqlCommand):Async<DBResult.DBResult<DataTable>> =
        async{
        let dt = new DataTable()
        // set a table name to avoid confusion when getting empty string back from ToString()
        dt.set_TableName "DBAction result"
        // use the reader so that it is closed when exiting
        // failing to close it will cause the error
        // Npgsql.NpgsqlOperationInProgressException: A command is already in progress
        // async here
        use! reader = cmd.ExecuteReaderAsync() |> Async.AwaitTask
        dt.Load(reader)
        return Ok [dt]
    }

    // For inserts and updates, returns the number of rows impacts, or -1 if unknown
    // Problem: there's no error reporting...
    let statementHandler  (cmd:NpgsqlCommand):Async<DBResult.DBResult<ImpactedRows>> =
        async {
        let! impact = cmd.ExecuteNonQueryAsync() |> Async.AwaitTask
        return Ok [ImpactedRows impact]
    }
