module SerialisableId
    // interface to be implemented by types whose value is internal or private (see EntityId)
    type SerialisableId =
        // The method returns the constructor (eg EntityId) and the value wrapped (the id's int value)
        abstract member getJSONInfo: unit -> {|name: string; value: int|}