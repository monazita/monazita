module MyOwnDB.Tests.Utils
// Utilities functions used in tests but unrelated to XUnit

open System.Data
open System.IO

open Testcontainers.PostgreSql
open Npgsql

// Prepare postgresql database on container
let issuePreparationQueries (container:PostgreSqlContainer) =
  let conn = new NpgsqlConnection(container.GetConnectionString())
  let queries =
      [
        "CREATE TABLE IF NOT EXISTS
        instances
        (
            id SERIAL,
            entity_id integer,
            created_at timestamp with time zone,
            lock_version integer DEFAULT 0,
            updated_at timestamp without time zone
        )"
        "CREATE TABLE IF NOT EXISTS
         accounts
         (
            id SERIAL,
            account_type_id integer,
            name text,
            street text,
            zip_code text,
            city text,
            country text,
            status text DEFAULT 'inactive'::text,
            end_date date DEFAULT now(),
            subscription_id text,
            subscription_gateway text,
            vat_number text,
            attachment_count integer DEFAULT 0,
            lock_version integer DEFAULT 0,
            created_at timestamp without time zone,
            last_login_at timestamp without time zone
          )
        "
        "CREATE TABLE IF NOT EXISTS
          users
          (
          id SERIAL,
          account_id integer NOT NULL,
          user_type_id integer DEFAULT 2,
          login character varying(80),
          password character varying,
          email character varying(40),
          firstname character varying(80),
          lastname character varying(80),
          uuid character(32),
          salt character(32),
          verified integer DEFAULT 0,
          created_at timestamp without time zone,
          updated_at timestamp without time zone,
          logged_in_at timestamp without time zone,
          api_key text,
          aspnet_pass_hash text
        )
        "

        "
        CREATE TABLE IF NOT EXISTS
          databases (
            id SERIAL,
            account_id integer NOT NULL,
            name text,
            lock_version integer DEFAULT 0
          );

        "
      ]
  queries
  |> List.iter (fun query ->
    let command = conn.CreateCommand()
    command.CommandText <- query
    if command.Connection.State <> ConnectionState.Open then command.Connection.Open()
    command.ExecuteNonQuery() |> ignore
  )
// build the postgresql container
let buildPostgresqlContainer () =
    (new PostgreSqlBuilder())
          .WithImage("postgres:16")
          .Build();
// Setup the postgresql container, ie start it and create tables
let initialisePostgresqlContainer (container:PostgreSqlContainer) = task {
    do! container.StartAsync()
    issuePreparationQueries(container)
}

// Setup and return the postgresql container
let setupPostgresql() = task {
    let container = buildPostgresqlContainer()
    do! initialisePostgresqlContainer container
    return container
}


let getScalarOption (client: DB.DBClient) query =
    let dt: DataTable =
        DBAction.staticDataTableAction query
        |> DBAction.run client
        |> Async.RunSynchronously
        |> DBResult.get
        |> List.head

    if dt.Rows.Count > 0 then Some(dt.Rows.[0].[0]) else None

let getScalar (client: DB.DBClient) query =
    match getScalarOption client query with
    | Some v ->
        if ((unbox v).GetType()) = typeof<System.DBNull>
        then failwith "scalar request returned NULL result"
        else (unbox v)
    | None -> failwith "no rows returned for scalar request. Check if you want to use getScalarDefaultZero"

let getScalarDefaultZero (client: DB.DBClient) query =
    match getScalarOption client query with
    | Some v ->
        if ((unbox v).GetType()) = typeof<System.DBNull>
        then 0
        else (unbox v)
    | None -> 0

let expectedJSON path =
    Monazita.Utils.reformatJSON (File.ReadAllText(path))

// function to be called at the start of a test like
//     _rollbackDisposable = setSavePoint Client.client
// It sets a savepoint and returns a disposable that will roll back
// to that savepoint. As it gets out of scope at the end of the test,
// this ensures that the next test runs on a clean database.
let setSavePoint (client:DB.DBClient) = async {
    let savePoint = "SAVEPOINT_StartOfTest"
    let! v = client.TrySetSavePoint savePoint
    v
    |> function
       | Ok [savePoint] -> ()
       | _ -> failwith "savepoint not set"


    let backToSavepoint () = async {
        let! v=client.TryToSavePoint savePoint
        return v
            |> function
               | Ok [()] -> ()
               | _ -> failwith "return to savepoint failed"
    }

    return Defer.deferAsync backToSavepoint
}
