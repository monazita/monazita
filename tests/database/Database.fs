namespace MyOwnDB.Tests.Database

open System
open Xunit
open FsUnit.Xunit
open MyOwnDB.Tests.Helpers
open DBTypes
open MyOwnDB.Extensions
open FSharp.Control.Tasks
open FSharpPlus
open Ids
// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =

    // The client can only be set after the database container is started by the fixtures
    let mutable clientOption : Option<DB.DBClient> = None
    let getClient() =
        clientOption|> Option.get


module TestFixtures =
  // Postgresql container used by this test
  let mutable postgresqlContainer:Option<Testcontainers.PostgreSql.PostgreSqlContainer> = None
  type TestFixtures() =
    interface IAsyncLifetime with
      member _.InitializeAsync() = task {
        let! (container,client)  = FixturesUtils.initialiseDatabase()
        postgresqlContainer <- Some container
        Client.clientOption <- Some client
      }
      member _.DisposeAsync() = task {
        return! FixturesUtils.disposeDatabase (postgresqlContainer |> Option.get) (Client.getClient())
      }

module Database =
    //


    // to issue DBAction sql queries to database
    let issuer<'T> action = DBAction.run<'T> (Client.getClient()) action
    // to issue DBAction returned by parameterless function f
    let issuerf f = DBAction.execute (Client.getClient()) f

    // we define some helper functions to be used in the tests
    // DBAction to retrieve 10 first users
    // Defined as a variable, we have to explicitely set types, as
    // variables cannot be left generic
    let getTenUsersAction: DBAction.DBAction<User.User> =
        DBAction.queryAction "select * from users order by id desc limit 10"
    // we can also define it as a DBAction returning function, which can be left generic.
    let getTenUsersFunction () =
        DBAction.queryAction "select * from users order by id desc limit 10"
    // DBAction returning function to get the country of the user's account.
    let getUserAccountCountry (user: User.User) =
        // if you specify an incorrect type, eg DBAction.query<int>, it will compile,
        // but you will get strange runtime errors like "Dapper , query interrupted at user request".
        let (AccountId accountId) = user.account_id
        DBAction.queryAction "select country from accounts where id = %d" accountId

    // some expected results put into variables
    let tenUsersEmails =
        [
            "fbasice1@example.com"
            "fbasicc1@example.com"
            "fbasica2@example.com"
            "fbasica1@example.com"
            "fbasic1@example.com"
            "frank@example.com"
            "admin2@test.com"
            "longbob@test.com"
            "existingbob@test.com"
            "bob@test.com"
        ]

    let tenCountries =
        [
            "Zimbabwe"
            "Belgium"
            "Russia"
            "Russia"
            "Zimbabwe"
            "Belgium"
            "Belgium"
            "Belgium"
            "Belgium"
            "Belgium"
        ]

    [<Trait("Category", "database")>]
    type DatabaseTests() =

        [<Fact>]
        member __.``Retrieving_database_records``() = async {
            do! getTenUsersAction
                |> issuer<User.User>
                |> Async.map (DBResult.map (fun u -> u.email))
                |> Async.map (checkOkResult tenUsersEmails)
        }

        [<Fact>]
        member __.``Testing DBAction.bind``() = async {
            // we need to specify we want the issuer to return strings, otherwise for each row it returns a sequence
            // seq [ country "countryString" ]
            do! getTenUsersAction
                |> DBAction.bind getUserAccountCountry
                |> issuer<string>
                |> Async.map (checkOkResult tenCountries)
        }

        [<Fact>]
        member __.``Immediate query with parameters``() = async {
            do! DBAction.immediate (Client.getClient()) "select * from users where id> %d and id<%d and email!=%s order by id limit %d" 1 4000 "hello@example.com" 10
                |> Async.map (DBResult.map (fun (u: User.User) -> let (UserId uid)=u.id
                                                                  uid
                                            ))
                |> Async.map (checkOkDBResult [ 2; 6; 100 ])
        }

        //---------------------------------------------------------------
        // Here come alternate ways to check rows count in the database
        [<Fact>]
        member __.``Immediate count query``() = async {
            do! DBAction.immediate (Client.getClient()) "select count(*) from users where id <10"
                |> Async.map (checkOkDBResult [ 2L ])
        }
        [<Fact>]
        member __.``Immediate count query to single result``() = async {
            do! DBAction.immediate (Client.getClient()) "select count(*) from users where id <10"
                |> Async.map (DBResult.toSingleResult)
                |> Async.map (checkOkResult 2L)
        }

        [<Fact>]
        member __.``Immediate count useing helper checkRowsCount``() = async {
            do! checkRowsCount (Client.getClient()) "users" "where id<10" 2L
        }

        //---------------------------------------------------------------
        // Populate anonymous records
        [<Fact>]
        member __.``Immediate select query to anonymous record``() = async {
            // can be assigned
            let usersAsync: Async<DBResult.DBResult<struct {| id: int; email: string |}>> =
                DBAction.immediate (Client.getClient()) "select id, email from users where id <10 order by id"
            let! users = usersAsync

            users
            |> checkOkDBResult
                [ {| id = 2
                     email = "admin@test.com" |}
                  {| id = 6
                     email = "selenium@raphinou.com" |} ]

            // can be piped directly, thanks to CheckOkDBResult's definition type annotation
            do! DBAction.immediate (Client.getClient()) "select id, email from users where id <10 order by id"
                |> Async.map (checkOkDBResult
                                [ {| id = 2
                                     email = "admin@test.com" |}
                                  {| id = 6
                                     email = "selenium@raphinou.com" |} ])

        }


        [<Fact>]
        member __.``Single result queries and DBAction.bind``() = async {
            // we will use single row actions to have successful and error generating DBActions
            let twoRowsAction =
                DBAction.querySingleAction "select * from users order by id desc limit 2"

            let oneRowAction =
                DBAction.querySingleAction "select * from users order by id desc limit 1"

            // check error corresponds to the unexpected number of rows when we us DBAction.bind afterwards
            do! twoRowsAction
                |> DBAction.bind getUserAccountCountry
                |> issuer
                |> Async.map (checkErrorResult [ "One row expected in result but got 2" ])

            // successful one row result action
            do! oneRowAction
                |> DBAction.bind getUserAccountCountry
                |> issuer<string>
                |> Async.map (checkOkResult [ "Zimbabwe" ])
        }

        [<Fact>]
        member __.``DBAction.map``() = async {
            // successful query
            // select  2 users
            do! DBAction.queryAction "select * from users order by id desc limit 2"
                // get their countries
                |> DBAction.bind getUserAccountCountry
                // convert the country name to uppercase
                |> DBAction.map (fun (c: string) -> c.ToUpper())
                |> issuer
                |> Async.map (checkOkResult [ "ZIMBABWE"; "BELGIUM" ])

            // error generating query
            do! DBAction.querySingleAction "select * from users order by id desc limit 2"
                // get their countries
                |> DBAction.bind getUserAccountCountry
                // convert the country name to uppercase
                |> DBAction.map (fun (c: string) -> c.ToUpper())
                |> issuer
                |> Async.map (checkErrorResult [ "One row expected in result but got 2" ])
        }

        // test name prefixed with Z to be the last to run
        [<Fact>]
        member __.``Z DBAction.Statement``() = async {
            // get record from database
                    do! DBAction.statementAction "delete from users where email=%s" "mohsinuser@company.it"
                        |> issuer
                        |> Async.map (checkOkResult [ ImpactedRows 1 ])
        }


        [<Fact>]
        member __.``datatable_and_json_format``() = async {

            // select 2 columns, check results
            do! DBAction.dataTableAction "select id,login from users where id<20 order by id"
                |> DBAction.mapDataTableToJSON
                |> DBAction.run (Client.getClient())
                |> checkOkDBResultAsync [ """[
  {
    "id": 2,
    "login": "admin@test.com"
  },
  {
    "id": 6,
    "login": "selenium@raphinou.com"
  }
]"""
                ]

            // select 3 columns, check results
            do! DBAction.dataTableAction "select id,login,email from users where id<20 order by id"
                |> DBAction.mapDataTableToJSON
                |> DBAction.run (Client.getClient())
                |> checkOkDBResultAsync [ """[
  {
    "id": 2,
    "login": "admin@test.com",
    "email": "admin@test.com"
  },
  {
    "id": 6,
    "login": "selenium@raphinou.com",
    "email": "selenium@raphinou.com"
  }
]"""
                ]

            // check datatable object received
            let! dt = DBAction.dataTableAction "select * from users"
                                |> DBAction.run (Client.getClient())
                                |> Async.map DBResult.get
                                |> Async.map List.head
            dt.Rows.Count |> should equal 13
            dt.Columns.Count |> should equal 16

            dt.Columns.GetEnumerator().toSeq()
            |> Seq.cast<System.Data.DataColumn>
            |> Seq.map (fun c-> c.ColumnName)
            |> Seq.toArray
            |>  should equal [| "id";"account_id";"user_type_id";"login";"password";"email";"firstname";"lastname";"uuid";"salt";"verified";"created_at";"updated_at";"logged_in_at";"api_key";"aspnet_pass_hash"|]

            dt.get_TableName() |> should equal "DBAction result"
        }

        interface IClassFixture<TestFixtures.TestFixtures>
