namespace MyOwnDB.Tests.SavePoints

open System
open Xunit
open System.IO
open FsUnit.Xunit
open MyOwnDB.Tests.Helpers
open DBTypes
open FSharp.Control.Tasks
open FSharpPlus

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    // The client can only be set after the database container is started by the fixtures
    let mutable clientOption : Option<DB.DBClient> = None
    let getClient() =
        clientOption|> Option.get

module Fixtures =
    // Postgresql container used by this test
    let mutable postgresqlContainer:Option<Testcontainers.PostgreSql.PostgreSqlContainer> = None
    type TestFixtures() =
      interface IAsyncLifetime with
        member _.InitializeAsync() = task {
          let! (container,client)  = FixturesUtils.initialiseDatabase()
          postgresqlContainer <- Some container
          Client.clientOption <- Some client
        }
        member _.DisposeAsync() = task {
          return! FixturesUtils.disposeDatabase (postgresqlContainer |> Option.get) (Client.getClient())
        }

module SavePoints =
    [<Trait("Category", "database")>]
    [<Trait("Category", "savepoints")>]
    type InstanceTests() =

        interface IClassFixture<Fixtures.TestFixtures>

        [<Fact>]
        member __.``DB.SavePoints``() = async {
            // get state before any change. We will then check that after we rollback
            // no change is present in the database
            let! preState, checkDifferenceFromInitialState =
                DBState.startWithCustomQueries (Client.getClient()) [] DBState.zeroState

            // at start, no savepoint is present
            (Client.getClient()).SavePoints()
            |> should equal (List<string>.Empty)

            // set first savepoint before any change
            do! (Client.getClient()).TrySetSavePoint "first"
                |> checkOkDBResultAsync [ "first" ]
            (Client.getClient()).SavePoints()
            |> should equal [ "first" ]


            // make first changes
            do! DBAction.recordStatementAction "delete from instances where id > 200 and id <= 100000" {|  |}
                |> DBAction.run (Client.getClient())
                |> checkOkDBResultAsync [ ImpactedRows 5 ]

            // check the delete instances are not there
            do! DBAction.immediate (Client.getClient()) "select count(*) from instances where id <= 100000"
                |> checkOkDBResultAsync [ 34L ]

            // take db state snapshot
            let! _, checkDifferenceWithSecondSavePoint =
                DBState.startWithCustomQueries (Client.getClient()) [] DBState.zeroState

            // set second savepoint
            do! (Client.getClient()).TrySetSavePoint "second"
                |> checkOkDBResultAsync [ "second" ]
            (Client.getClient()).SavePoints()
            |> should equal [ "second"; "first" ]

            // delete remaining instances
            do! DBAction.recordStatementAction "delete from instances where id <= 100000" {|  |}
                |> DBAction.run (Client.getClient())
                |> checkOkDBResultAsync [ ImpactedRows 34 ]

            // check the delete instances are not there
            do! DBAction.immediate (Client.getClient()) "select count(*) from instances where id<= 100000"
                |> checkOkDBResultAsync [ 0L ]

            // go back one savepoint,
            do! (Client.getClient()).TryToSavePoint "second"
                |> checkOkDBResultAsync [ () ]
            (Client.getClient()).SavePoints()
            |> should equal [ "first" ]

            // check that compared to the db state at second savepoint, there's no difference
            do! checkDifferenceWithSecondSavePoint ()

            // got back to first savepoint
            do! (Client.getClient()).TryToSavePoint "first"
                |> checkOkDBResultAsync [ () ]
            // check there's no savepoint left
            (Client.getClient()).SavePoints()
            |> should equal (List<string>.Empty)

            // check we are back at the initial state
            do! DBAction.immediate (Client.getClient()) "select count(*) from instances where id <=  100000"
                |> checkOkDBResultAsync [ 39L ]
            do! checkDifferenceFromInitialState ()
        }
