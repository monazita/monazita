namespace MyOwnDB.Tests.DBTestsWithoutFixtures

open Xunit
open MyOwnDB.Tests.Helpers
open System
open DBAction
open DBAction.StaticArguments
open DBTypes
open FSharp.Control.Tasks
open FSharpPlus
open MyOwnDB.Tests.Utils

module Client =
    // The client can only be set after the database container is started by the fixtures
    let mutable clientOption : Option<DB.DBClient> = None
    let getClient() =
        clientOption|> Option.get


module PlainDatabase =
    type Setup() =

        // Postgresql container used by this test
        let mutable postgresqlContainer:Option<Testcontainers.PostgreSql.PostgreSqlContainer> = None
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! container = setupPostgresql()
                let client = DB.DBClient(container.GetConnectionString())
                postgresqlContainer <- Some container
                Client.clientOption <- Some client

                // Load fixtures
                let! () = client.Open()
                let! impactedRows =
                    DBAction.statementAction "create table IF NOT EXISTS test_table(id SERIAL,value text)"
                    |>DBAction.run client
                match impactedRows with
                | Ok n -> ()
                | Error es -> failwithf "Error creating table for plain database tests: %A" es

            }

            member __.DisposeAsync() = unitTask {
                let! impactedRows =
                    DBAction.statementAction "drop table test_table"
                    |>DBAction.run (Client.getClient())
                match impactedRows with
                | Ok n -> ()
                | Error es -> failwithf "Error dropping table for plain database tests: %A" es
                let! () = (Client.getClient()).Close()
                return ()
            }
module DBActionWithoutFixtures =
    // to issue DBAction sql queries to database
    let issuer<'T> action = DBAction.run<'T> (Client.getClient()) action

    let buildValueAction (s:string)= DBAction.statementAction "insert into test_table(value) VALUES (%s)" s

    let thenRun nextAction action =
        action
        |>DBAction.bind (fun _ -> nextAction)

    let checkRows (n:int64) =
        DBAction.immediate (Client.getClient()) "select count(*) from test_table"
        |> Async.map (checkOkDBResult  [n])

    let cleanTable () = async {
        do! DBAction.statementAction "delete from test_table"
            |> run (Client.getClient())
            |> Async.Ignore
        do! checkRows 0
    }

    type RowRecord =
        {
            id : int
            value: string
        }

    open FsUnit.Xunit
    [<Trait("Category", "database")>]
    [<Trait("Category", "dbaction")>]
    type DBActionTests() =

        interface IClassFixture<PlainDatabase.Setup>

        //************************************************************
        [<Fact>]
        member __.``dbaction_wrapInTx``() = async {

            // put all our insert actions in a list
            let insertActions =
                [ buildValueAction "value0"
                  buildValueAction "value1"
                  buildValueAction "value2"
                  buildValueAction "value3"
                  buildValueAction "value4"
                  buildValueAction "value5"
                  buildValueAction "value6"
                ]
            let errorAction = DBAction.error "my error action"


            // First a text without tx to check actions wnd statements are working as expected
            // insert our test rows
            do! insertActions[0]
                |> thenRun (insertActions[1])
                |> thenRun (insertActions[2])
                |> thenRun (insertActions[3])
                |> DBAction.run (Client.getClient())
                // ImpactedRows is 1 because it is the result of the last insertion
                |> Async.map (checkOkDBResult  [ImpactedRows 1])
            // check all 4 were inserted
            do! checkRows 4L
            do! cleanTable()

            // Wrap succesful actions with transaction
            do! insertActions[0]
                |> thenRun (insertActions[1])
                |> thenRun (insertActions[2])
                |> thenRun (insertActions[3])
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkOkDBResult  [ImpactedRows 1])
            do! checkRows 4L
            do! cleanTable()

            // Wrap succesful actions with transaction
            do! insertActions[0]
                |> thenRun (insertActions[1])
                |> thenRun (insertActions[2])
                |> thenRun (errorAction)
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkErrorResult  ["my error action"])
            do! checkRows 0L
            do! cleanTable()

            // Wrap succesful actions with transaction when a transaction is already active
            // In this case, the existing transaction should not be committed
            do! (Client.getClient()).BeginTransaction()
            do! insertActions[0]
                |> thenRun (insertActions[1])
                |> thenRun (insertActions[2])
                |> thenRun (insertActions[3])
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkOkDBResult  [ImpactedRows 1])
            // check the transaction was not committed
            (Client.getClient()).IsInTransaction() |> should equal true
            // in the transaction, we see 4 rows inserted
            do! checkRows 4L
            // rollback transaction and check the insertions are not present
            do! (Client.getClient()).TryRollback() |> Async.Ignore
            (Client.getClient()).IsInTransaction() |> should equal false
            do! checkRows 0L
            do! cleanTable()

            // Wrap error actions with transaction when a transaction is already active
            // In this case, the existing transaction should not be rolled back
            do! (Client.getClient()).BeginTransaction()
            do! insertActions[0]
                |> thenRun (insertActions[1])
                |> thenRun (insertActions[2])
                |> thenRun (errorAction)
                |> thenRun (insertActions[3])
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkErrorResult  ["my error action"])
            // check the transaction was not committed
            (Client.getClient()).IsInTransaction() |> should equal true
            // in the transaction, we see 3 rows inserted for the 3 successful action before the error occured
            do! checkRows 3L
            // rollback transaction and check the insertions are not present
            do! (Client.getClient()).TryRollback() |> Async.Ignore
            (Client.getClient()).IsInTransaction() |> should equal false
            do! checkRows 0L
            do! cleanTable()


            // *********************** Wrap also with savepoints ********************

            // Wrap succesful actions with transaction. All should go smoothly here
            do! insertActions[0]
                |> thenRun (insertActions[1])
                |> thenRun (insertActions[2])
                |> thenRun (insertActions[3])
                |> DBAction.wrapWithSavePointAndRollBackForError
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkOkDBResult  [ImpactedRows 1])
            // check the transaction was not committed
            (Client.getClient()).IsInTransaction() |> should equal false
            do! checkRows 4L
            do! cleanTable()

            // Wrap actions with transaction and savepoint wrapped actions.
            // We have an action outside the savepoint wrapped actions, but still in the transaction.
            // all should be ok here
            do! insertActions[0]
                |> thenRun (insertActions[1])
                |> thenRun (insertActions[2])
                |> DBAction.wrapWithSavePointAndRollBackForError
                |> thenRun (insertActions[3])
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkOkDBResult  [ImpactedRows 1])
            // check the transaction was not committed
            (Client.getClient()).IsInTransaction() |> should equal false
            do! checkRows 4L
            do! cleanTable()

            // Wrap actions with transaction and an error action in the savepoint wrapped actions.
            // in this case actions nothing should be inserted.
            // the wrapInTransactionIfNeeded call gets an error for a transaction it started, so it rolls
            // it back
            do! insertActions[0]
                |> thenRun (insertActions[1])
                |> thenRun (errorAction)
                |> DBAction.wrapWithSavePointAndRollBackForError
                |> thenRun (insertActions[3])
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkErrorResult  ["my error action"])
            // check the transaction was not committed
            (Client.getClient()).IsInTransaction() |> should equal false
            do! checkRows 0L
            do! cleanTable()

            // Wrap actions with transaction and an error action in the savepoint wrapped actions.
            // A rollback to savepoint occurs, and nothing else is inserted because we have an error.
            // the wrapInTransactionIfNeeded call gets an error for a transaction it started, so it rolls
            // it back
            do! (Client.getClient()).BeginTransaction()
            do! insertActions[0]
                |> thenRun (insertActions[1])
                |> thenRun (errorAction)
                |> DBAction.wrapWithSavePointAndRollBackForError
                |> thenRun (insertActions[3])
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkErrorResult  ["my error action"])
            // check the transaction was not committed
            (Client.getClient()).IsInTransaction() |> should equal true
            do! checkRows 0L
            do! (Client.getClient()).TryRollback() |> Async.Ignore
            do! cleanTable()

            // Wrap actions with transaction and an error action outside the savepoint wrapped actions.
            // in this case actions everything before the error occurs is inserted in the database
            // because there was an existing transaction.
            do! (Client.getClient()).BeginTransaction()
            do! insertActions[0]
                |> thenRun (insertActions[1])
                |> DBAction.wrapWithSavePointAndRollBackForError
                |> thenRun (insertActions[2])
                |> thenRun (errorAction)
                |> thenRun (insertActions[3])
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkErrorResult  ["my error action"])
            // check the transaction was not committed
            (Client.getClient()).IsInTransaction() |> should equal true
            do! checkRows 3L
            do! (Client.getClient()).TryRollback() |> Async.Ignore
            do! cleanTable()


            // This tests a savepoint wrapped action in the middle of other actions.
            // All succesful, so all should be ok
            do! (Client.getClient()).BeginTransaction()
            let actionWithSavePoint =
                insertActions[1]
                |> thenRun (insertActions[2])
                |> DBAction.wrapWithSavePointAndRollBackForError

            do! insertActions[0]
                |> thenRun (actionWithSavePoint)
                |> thenRun (insertActions[3])
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkOkDBResult  [ImpactedRows 1])
            // check the transaction was not committed
            (Client.getClient()).IsInTransaction() |> should equal true
            do! checkRows 4L
            do! (Client.getClient()).TryRollback() |> Async.Ignore
            do! cleanTable()

            // This tests a savepoint wrapped action in the middle of other actions.
            // The savepoint wrapped action is an error, so this should be rolled back.
            // And everything after this is not inserted either because we get an error result.
            do! (Client.getClient()).BeginTransaction()
            let actionWithSavePoint =
                insertActions[1]
                |> thenRun (errorAction)
                |> DBAction.wrapWithSavePointAndRollBackForError

            do! insertActions[0]
                |> thenRun (actionWithSavePoint)
                |> thenRun (insertActions[2])
                |> thenRun (insertActions[3])
                // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run (Client.getClient())
                |> Async.map (checkErrorResult  ["my error action"])
            // check the transaction was not committed
            (Client.getClient()).IsInTransaction() |> should equal true
            do! checkRows 1L
            do! (Client.getClient()).TryRollback() |> Async.Ignore
            do! cleanTable()

            // Test multiple savepoints that are rolled back independently and don't propagate
            // their errors so subsequent actions are run
            let insertWithSavepointAction =
              insertActions[0]
              |> thenRun (insertActions[1] )
              |> thenRun (insertActions[2] )
              |> thenRun
                (insertActions[3]
                 |> thenRun (insertActions[4])
                 |> thenRun (DBAction.error "failed action")
                 |> DBAction.wrapWithSavePointAndRollBackForErrorButContinue
                )
              |> thenRun
                (insertActions[5]
                 |> thenRun (insertActions[6])
                 |> DBAction.wrapWithSavePointAndRollBackForErrorButContinue
                )
              |> DBAction.wrapInTransactionIfNeeded

            do! insertWithSavepointAction
                |> DBAction.run (Client.getClient())
                |> Async.Ignore
            do! checkRows 5L
            do! DBAction.queryAction "select value from test_table"
                |> DBAction.run (Client.getClient())
                |> Async.map (checkOkDBResult ["value0";"value1";"value2";"value5";"value6"])
            do! cleanTable()

            // Test multiple savepoints that are rolled back independently and propagate
            // their errors so subsequent actions are NOT run and tx is rolled back
            let insertWithSavepointAction =
              insertActions[0]
              |> thenRun (insertActions[1] )
              |> thenRun (insertActions[2] )
              |> thenRun
                (insertActions[3]
                 |> thenRun (DBAction.error "failed action")
                 |> DBAction.wrapWithSavePointAndRollBackForError
                )
              |> thenRun
                (insertActions[5]
                 |> thenRun (insertActions[6])
                 |> DBAction.wrapWithSavePointAndRollBackForError
                )
              |> DBAction.wrapInTransactionIfNeeded

            do! insertWithSavepointAction
                |> DBAction.run (Client.getClient())
                |> Async.Ignore
            do! checkRows 0L
            do! cleanTable()
        }

        [<Fact>]
        member __.``txTracking``() = async {
            // Check that transactions are tracked correctly when comitting and rolling back
            do! (Client.getClient()).BeginTransaction()
            (Client.getClient()).IsInTransaction() |> should equal true
            do! (Client.getClient()).Commit()
            (Client.getClient()).IsInTransaction() |> should equal false

            do! (Client.getClient()).BeginTransaction()
            (Client.getClient()).IsInTransaction() |> should equal true
            do! (Client.getClient()).TryRollback() |> Async.Ignore
            (Client.getClient()).IsInTransaction() |> should equal false

        }

        [<Fact>]
        member __.``singular_table_names_ids``() = async {
           // This validates that we handle singular table names correctly when we try to build their
           // id types.
           do! DBAction.queryAction "insert into test_table(value) VALUES ('mystring') RETURNING *"
               |> DBAction.map (fun (row:RowRecord) -> (row.id,row.value) )
                |> DBAction.run (Client.getClient())
                |> Async.map (checkOkDBResult  [(1,"mystring")])

           do! cleanTable()
        }
