namespace MyOwnDB.Tests.Error

open Xunit
open MyOwnDB.Tests.Helpers
open FsUnit.Xunit
open Error

module Error =

    [<Trait("Category", "database")>]
    [<Trait("Category", "error")>]
    type CrosstabTests() =

        [<Fact>]
        member __.``error.join``() =

            Error.join (Error.fromString "first") (Error.fromString "second")
            // wrap in Error case of Result, so we can use our test helper
            |> Error
            |> checkErrorResult ["first";"second"]

        [<Fact>]
        member __.``error.toStringListAndString``() =
            let (<*>) = join
            let exn1 = (System.Exception( "my first exn"))
            let exn2 = (System.Exception( "my second exn"))
            let error1 = (Error.fromString "my first string")
            let error2 = error1 <*> (Error.fromExn exn1 )
            let error3 = error2 <*> (Error.fromExn exn2 )
            let error4 = error3 <*> (Error.fromString "my second string")

            error1 |> Error.toStringList |> should equal ["my first string"]
            error2 |> Error.toStringList |> should equal ["my first string"; "System.Exception: my first exn"]
            error3 |> Error.toStringList |> should equal ["my first string"; "System.Exception: my first exn"; "System.Exception: my second exn"]
            error4 |> Error.toStringList |> should equal ["my first string"; "System.Exception: my first exn"; "System.Exception: my second exn"; "my second string"]

            error1 |> Error.toString |> should equal "my first string"
            error2 |> Error.toString |> should equal "my first string\nSystem.Exception: my first exn"
            error3 |> Error.toString |> should equal "my first string\nSystem.Exception: my first exn\nSystem.Exception: my second exn"
            error4 |> Error.toString |> should equal "my first string\nSystem.Exception: my first exn\nSystem.Exception: my second exn\nmy second string"

        [<Fact>]
        member __.``error.getExceptions``() =
            let (<*>) = join
            let exn1 = (System.Exception( "my first exn"))
            let exn2 = (System.Exception( "my second exn"))
            let error1 = (Error.fromString "my first string")
            let error2 = error1 <*> (Error.fromExn exn1 )
            let error3 = error2 <*> (Error.fromExn exn2 )
            let error4 = error3 <*> (Error.fromString "my second string")

            error1 |> Error.getExceptions |> should equal List<exn>.Empty
            error2 |> Error.getExceptions |> should equal [exn1]
            error3 |> Error.getExceptions |> should equal [exn1;exn2]
            error4 |> Error.getExceptions |> should equal [exn1;exn2]
