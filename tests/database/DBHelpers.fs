namespace MyOwnDB.Tests.DBHelpers

open System
open Xunit
open FsUnit.Xunit
open MyOwnDB.Tests.Helpers
open DBTypes
open MyOwnDB.Extensions
open FSharp.Control.Tasks
open FSharpPlus
// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module Client =
    // The client can only be set after the database container is started by the fixtures
    let mutable clientOption : Option<DB.DBClient> = None
    let getClient() =
        clientOption|> Option.get


module Fixtures =
    type DBHelperFixture() =
        // Postgresql container used by this test
        let mutable postgresqlContainer:Option<Testcontainers.PostgreSql.PostgreSqlContainer> = None
        interface IAsyncLifetime with
          member _.InitializeAsync() = task {
            let! (container,client)  = FixturesUtils.initialiseDatabase()
            postgresqlContainer <- Some container
            Client.clientOption <- Some client
          }
          member _.DisposeAsync() = task {
            return! FixturesUtils.disposeDatabase (postgresqlContainer |> Option.get) (Client.getClient())
          }

module Database =
    // to issue DBAction sql queries to database
    let issuer<'T> action = DBAction.run<'T> (Client.getClient()) action
    // to issue DBAction returned by parameterless function f
    let issuerf f = DBAction.execute (Client.getClient()) f

    // Define types that we will use in the test below to validate
    // mappings to types
    type Protocol = |Smtp |Http
    type AddressId = AddressId of int
    type Address =
      {
        id: AddressId option
        protocol: Protocol
        protocolOption: Protocol option
        protocolOption2: Protocol option
        protocolString : string
        protocolString2 : string
      }

    type AccountId = AccountId of int
    type DatabaseId = DatabaseId of int
    module Specs =
      type DatabaseSpec =
        { databaseId: DatabaseId option
          accountId: AccountId
          name: string
        }

    [<Trait("Category", "dbhelpers")>]
    type DatabaseTests() =
        interface IClassFixture<Fixtures.DBHelperFixture>
        [<Fact>]
        member __.``helper_mapping_records``() = async {
            do! DBAction.queryAction "select id as DatabaseId,account_id as AccountId, name from databases where id=6"
                |> issuer<Specs.DatabaseSpec>
                |> Async.map (checkOkResult [{databaseId=Some (DatabaseId 6); accountId = AccountId 1; name = "DemoForTests"}])
        }

        [<Fact>]
        member __.``helper_mapping_records_with_option_ids``() = async {
            do! DBAction.queryAction "select null as DatabaseId,account_id as AccountId, name from databases where id=6"
                |> issuer<Specs.DatabaseSpec>
                |> Async.map (checkOkResult [{databaseId=None; accountId = AccountId 1; name = "DemoForTests"}])
        }

        [<Fact>]
        member __.``mapping_valueless_cases``() = async {
            do! DBAction.queryAction "select 34 as id,'smtp' as protocol, 'smtp' as protocolOption, null as protocolOption2, 'smtp' as protocolString, '' as protocolString2"
                |> issuer<Address>
                |> Async.map (checkOkResult [{id=Some (AddressId 34); protocol = Smtp; protocolOption = Some Smtp; protocolOption2 = None; protocolString = "smtp"; protocolString2 = ""}])
        }
