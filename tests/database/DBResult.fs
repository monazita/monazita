namespace MyOwnDB.Tests.DBResult

open Xunit
open FsUnit.Xunit
open MyOwnDB.Tests.Helpers
open DBResult

module DBResult =

    [<Trait("Category", "database")>]
    [<Trait("Category", "dbresult")>]
    type CrosstabTests() =

        [<Fact>]
        member __.``dbaction_apply``() =
            let f x = x+2
            let dbresult = wrapList [1;2;3]
            let error1Result = error "error 1"
            let error2Result = error "error 2"
            let errorfn:DBResult<int->int> = error "error fn"
            let (<*>) = apply

            retn f <*> dbresult
            |> checkOkDBResult [3;4;5]

            retn f <*> error1Result
            |> checkErrorResult [ "error 1" ]

            errorfn <*> dbresult
            |> checkErrorResult [ "error fn" ]

            errorfn <*> error2Result
            |> checkErrorResult [ "error fn";"error 2" ]

        member __.``dbaction_wrapList``() =
            wrapList [1;2;3]
            |> checkOkDBResult [1;2;3]

            wrapList ["un";"dos";"tres"]
            |> checkOkDBResult ["un";"dos";"tres"]

        [<Fact>]
        member __.``dbaction_merge``() =

            merge (wrapList [1;2;3]) (wrapList [4;5;6])
            |> checkOkDBResult [1;2;3;4;5;6]

            merge (wrapList ["un";"dos"]) (wrapList ["tres"])
            |> checkOkDBResult ["un";"dos";"tres"]

        [<Fact>]
        member __.``dbresult_flatten``() =

            flatten (wrapList [[1];[2;3]])
            |> checkOkDBResult [1;2;3;]

            flatten (wrapList [[[1]];[[2];[3]]])
            |> checkOkDBResult [[1];[2];[3];]

        [<Fact>]
        member __.``dbresult_zip``() =

            zip (wrapList [1;2;3]) (wrapList ["a";"b";"c"])
            |> checkOkDBResult [(1,"a");(2,"b");(3,"c")]

            zip (wrapList [1;2;3]) (wrapList [1;2;3])
            |> checkOkDBResult [(1,1);(2,2);(3,3)]

        [<Fact>]
        member __.``dbresult_map``() =

            map (fun v -> v+1) (wrapList [1;2;3])
            |> checkOkDBResult [2;3;4]

        [<Fact>]
        member __.``dbresult_toresult``() =

            toResult (wrapList [1;2;3])
            |> function
                | Ok l -> l |> should equal [1;2;3]
                | _ -> failwith "unexpected result"

            toResult (error "sample error")
            |> function
                | Ok _ -> failwith "unexpected Ok result"
                | Error e -> e |> should equal ["sample error"]

        [<Fact>]
        member __.``dbresult_mapError``() =

            mapError (fun e -> Error.fromString $"my new error from: {Error.toString e}") (wrapList [1;2;3])
            |> checkOkDBResult [1;2;3;]
            mapError (fun e -> Error.fromString $"my new error from: {Error.toString e}") (error "my initial error")
            |> checkErrorResult ["my new error from: my initial error"]
