module FixturesUtils

open Testcontainers.PostgreSql
open MyOwnDB.Tests.Utils

let initialiseDatabase () = task{
    let! container = setupPostgresql()
    let client = DB.DBClient(container.GetConnectionString())

    // Load fixtures
    let! () = client.Open()
    let! () = client.BeginTransaction()
    let fixtures =
            [| "Account+Account,tests"
               "User+User,tests"
               "Instance+Instance,tests"
               "Database+Database,tests"
            |]
    let! _ = Fixtures.processRecordsArray client fixtures |> Async.Sequential
    return (container,client)
}

let disposeDatabase (container:PostgreSqlContainer)(client:DB.DBClient) = task {
        let! _ = client.TryRollback()
        let! () = client.Close()
        do! container.DisposeAsync().AsTask()

}
