namespace MyOwnDB.Tests.FsHelpers

open Xunit
open MyOwnDB.Tests.Helpers
open System
open DBAction
open DBTypes
open FSharp.Control.Tasks
open FSharpPlus
open Xunit
open FsUnit
open Helpers

module HelpersTests =
    // to issue DBAction sql queries to database

    [<Trait("Category", "helpers")>]
    type HelpersTests() =

        let mutable cell = 0
        // our log function increments a cell, so we can check it was called or not
        let logFunction () =
            cell <- cell + 1
        let getLogCallsChecker () =
            let previous = cell
            fun (expected:int) ->
                let count = cell - previous
                count |> should equal expected
        let errorMessage = "Error message if None"

        [<Fact>]
        member __.``helpers_resultofoption``() =
            let logChecker = getLogCallsChecker ()
            None
            |> resultOfOption logFunction errorMessage
            |> function
               | Error [errorMessage] -> ()
               | v -> v |> should equal false
            logChecker 1

            let logChecker = getLogCallsChecker ()
            Some 42
            |> resultOfOption logFunction errorMessage
            |> function
               | Ok 42 -> ()
               | v -> v |> should equal false
            logChecker 0

        [<Fact>]
        member __.``helpers_tuplesequencetoresult``() =
            ( Ok 1, Ok 2)
            |> tupleSequenceToResult
            |> should equal (Ok (1,2))

            ( Error "error 1", Ok 2)
            |> tupleSequenceToResult
            |> function
                | Error "error 1" -> ()
                | v ->  v |> should equal false

            ( Ok 1, Error "error 2")
            |> tupleSequenceToResult
            |> function
                | Error "error 2" -> ()
                | v ->  v |> should equal false

            ( Error "error 1", Error "error 2")
            |> tupleSequenceToResult
            |> function
                | Error "error 1" -> ()
                | v ->  v |> should equal false
