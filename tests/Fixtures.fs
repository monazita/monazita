module Fixtures

open System.IO
open System.Reflection
open Microsoft.FSharp.Reflection
open System
open Humanizer
open MyOwnDB.Tests.Utils

/// Define a lock for thread-unsafe code in Legivel
/// At least the Deserialize method has to be protected
let fixturesLock = ref 0

let fixtureTableName (fixture: string) = fixture.Replace("Fixture", "").Pluralize().Underscore()

let typeTableName (typ: System.Type) = fixtureTableName typ.Name

let insertStatementForType (typ: System.Type) =
    let props = typ.GetProperties() |> Array.map (fun p -> p.Name)
    let columns = props |> String.concat ", "

    let parms =
        props
        |> Array.map (fun p -> "@" + p)
        |> String.concat ", "
    // If we used the record type named with suffix Fixture to work around the legivel limitation (no
    // mapping to DU in record type), we need to remove it from the table name
    // Also set the value of the sequence used to assign id values, so insertions in tests will
    // not clash with existing records. The sequence value is not updated because fixtures are inserted
    // specifying the id column's value, preventing the use of the sequence.
    sprintf "INSERT INTO %s (%s) VALUES (%s);" (typeTableName typ) columns parms

let updateIdSequence (client: DB.DBClient) (typ: System.Type) = async {
    let table = typeTableName typ
    let maxIdQuery = sprintf "select max(id) from %s" table
    let maxId = getScalar client maxIdQuery
    let sequenceStatement = sprintf "select setval('%s_id_seq',200000);" table
    let! v = client.executeStatementOnRecord (sequenceStatement, {|  |})
    match v with
    | Ok _ -> return ()
    | Error e ->
        let msg = sprintf "update of sequence for table %s failed with error %s" (typeTableName typ) (e.ToString())
        return failwith msg
}

let insertStatement<'a>() =
    let recordType = typeof<'a>
    insertStatementForType recordType

let deserializeFixtureInDir<'a>(directory) =
    let filePath = sprintf "%s/%s.yml" directory ((typeof<'a>.Name.Replace("Fixture", "").Pluralize().Underscore()))
    let yaml = File.ReadAllText(filePath)
    // prevent concurrent execution of LegivelSerialization.Deserialize
    lock fixturesLock (fun () -> Legivel.Serialization.Deserialize<Map<string, 'a>>(yaml))

let deserializeFixture<'a>(directory) =
    deserializeFixtureInDir<'a>("fixtures")

let insertionIteratorOfType<'a> (client: DB.DBClient) =
    fun (k: string) (v: 'a) -> async {
        let statement = insertStatement<'a>()
        let! r=client.executeStatementOnRecord (statement, v)
        match r with
        | Ok _ -> return ()
        | Error e ->
            let msg =
                sprintf "statement %s failed with \n %s" statement (e.ToString())
            return failwith msg
    }

let handleResult r =
    match r with
    | Legivel.Serialization.Success info :: _ -> info.Data
    | Legivel.Serialization.Error e :: _ -> failwithf "YamlParse Error %A" e
    | [] -> Map.empty

let instanciateGeneric funcName (types: Type []) =
    Assembly.GetExecutingAssembly().GetType("Fixtures").GetMethod(funcName).MakeGenericMethod(types)

let recordsSeq<'a> =
    let records = deserializeFixture<'a>() |> handleResult
    Map.toSeq<string, 'a> records |> Seq.map fst

let processRecordsInDir<'a> client directory= async {
    let records = deserializeFixtureInDir<'a>(directory) |> handleResult
    let parallelAsyncs = Map.map<string, 'a, Async<unit>> (insertionIteratorOfType<'a> client) records
                         |> Map.toSeq
                         |> Seq.map (fun (k,v) -> v)
    // update the table's id assigning sequence
    let typ = typeof<'a>
    let updateAsync = seq {updateIdSequence client typ}
    return! Seq.concat [parallelAsyncs;updateAsync] |> Async.Sequential
}

let processRecordsArgsInDir (client: DB.DBClient) (t: Type) (directory:string)= async {
    let processRecordInstance = instanciateGeneric "processRecordsInDir" [| t |]
    let result = processRecordInstance.Invoke(null, [| client; directory |]) :?> Async<unit array>
    return! result
}

let processRecordsArrayInDir client (names: string []) (directory:string) =
    names
    |> Array.map (fun typeName ->
        // Legivel cannot map scalar types in the yaml to a discriminated union in fsharp
        // We work around it by defining a custom record type whose name is suffexed by Fixture
        // Here we try the Fixture record type first, and if it returns null, it means we didn't specify a custom
        // type, so we can use the normal one
        let recordType =
            match System.Type.GetType(typeName.Replace(",", "Fixture,"), false) with
            | null -> System.Type.GetType(typeName)
            | result -> result
        processRecordsArgsInDir client recordType directory
    )
let processRecordsArray client (names: string []) =
    processRecordsArrayInDir client names "fixtures"

let processRecordsTypesInDir client (names: Type []) (directory:string)=
    names |> Array.map (fun recordType -> processRecordsArgsInDir client recordType directory)

let getRecordsMap (fixture: string) =
    let filePath = sprintf "fixtures/%s.yml" ((fixture.Pluralize().Underscore()))

    let yaml = File.ReadAllText(filePath)
    let typeName = sprintf "%s+%s" fixture fixture


    let recordType = System.Type.GetType(typeName)

    let genericMapType = typedefof<Map<_, _>>

    let yamlType =
        genericMapType.MakeGenericType
            ([| typeof<string>
                recordType |])

    let legivelAssembly = Assembly.GetAssembly(typeof<Legivel.Serialization.DeserializeResult<_>>)
    let deserializeMethod = legivelAssembly.GetType("Legivel.Serialization").GetMethod("Deserialize")
    let genericMethod = deserializeMethod.MakeGenericMethod(yamlType)

    // prevent concurrent execution of LegivelSerialization.Deserialize
    lock fixturesLock (fun () -> genericMethod.Invoke(null, [| yaml |]))
