module User
open Ids
open System

type User =
    { id: UserId
      account_id: AccountId
      user_type_id: int
      login: string
      password: string
      aspnet_pass_hash: string option
      email: string
      firstname: string option
      lastname: string option
      uuid: string option
      salt: string
      verified: int
      created_at: DateTime
      updated_at: DateTime option
      logged_in_at: DateTime option
      api_key: string option }

type UserFixture =
    { id: int
      account_id: int
      user_type_id: int
      login: string
      password: string
      aspnet_pass_hash: string option
      email: string
      firstname: string option
      lastname: string option
      uuid: string option
      salt: string
      verified: int
      created_at: DateTime
      updated_at: DateTime option
      logged_in_at: DateTime option
      api_key: string option }
