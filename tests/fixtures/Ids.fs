module Ids

type UserId = UserId of int
type AccountId = AccountId of int
type InstanceId = InstanceId of int
type EntityId = EntityId of int
type DatabaseId = DatabaseId of int
