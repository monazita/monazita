module Instance

open Ids
open System
type Instance =
    { id: InstanceId
      entity_id: EntityId
      created_at: DateTime option
      lock_version: int
      updated_at: DateTime option }
type InstanceFixture =
    { id: int
      entity_id: int
      created_at: DateTime option
      lock_version: int
      updated_at: DateTime option }
